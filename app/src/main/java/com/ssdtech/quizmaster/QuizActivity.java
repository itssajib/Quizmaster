package com.ssdtech.quizmaster;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.ssdtech.quizmaster.entities.QuestionObject;
import com.ssdtech.quizmaster.entities.ResultWrapper;
import com.ssdtech.quizmaster.entities.UserCoins;
import com.ssdtech.quizmaster.entities.UserObject;
import com.ssdtech.quizmaster.network.GsonRequest;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.DisplayMessage;
import com.ssdtech.quizmaster.utils.Helper;
import com.ssdtech.quizmaster.utils.PlayAudio;
import com.ssdtech.quizmaster.utils.PlaySound;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.ssdtech.quizmaster.utils.DisplayMessage.displayErrorMessage;


public class QuizActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = QuizActivity.class.getSimpleName();

    private TextView question_num, tvPoint, quiz_question;
    private ProgressBar timeProgress;
    private ImageView question_image;
    private Button answerOneButton, answerTwoButton, answerThreeButton, answerFourButton;

    private String selectedQuizName;
    private String indicator="";
    private List<QuestionObject> questions;
    private QuestionObject currentQuestion;
    private int questionIndex = 0;
    private int questionNumber;
    private int totalQuestion;

    private Handler handler;
    private String question_path;

    private PlaySound playMusic;
    private PlayAudio playAudio;

    private List<ResultWrapper> resultList;
    private CountDownTimer countDownTimer;
    private int counter = 0;
    long timeCount=0,timeCount2=0;
    public Timer T,T2;
    TextView tv_time;
    long point=0,pointPerAnswer=5;

    private Gson gson;

    private boolean isMusicActive;
    private boolean isSoundActive;
    private UserObject userDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        T=new Timer();
        T2=new Timer();

        resultList = new ArrayList<ResultWrapper>();

        isMusicActive = ((CustomApplication) getApplication()).getShared().getSavedMusic();
        isSoundActive = ((CustomApplication) getApplication()).getShared().getSavedSound();
        tv_time= (TextView) findViewById(R.id.tv_time);

        playMusic = new PlaySound(QuizActivity.this);
        playAudio = new PlayAudio(QuizActivity.this);
        if (isMusicActive) {
            playAudio.playSound(R.raw.quizmusic);
        }

        gson = ((CustomApplication) getApplication()).getGsonObject();

        handler = new Handler();

        question_num = (TextView) findViewById(R.id.quiz_question_number);
        tvPoint = (TextView) findViewById(R.id.quiz_name);
        tvPoint.setText("0");
        quiz_question = (TextView) findViewById(R.id.quiz_question);

        question_image = (ImageView) findViewById(R.id.question_with_image);

        timeProgress = (ProgressBar) findViewById(R.id.quiz_timer);
        timeProgress.setMax(20);

        answerOneButton = (Button) findViewById(R.id.answer_one);
        answerTwoButton = (Button) findViewById(R.id.answer_two);
        answerThreeButton = (Button) findViewById(R.id.answer_three);
        answerFourButton = (Button) findViewById(R.id.answer_four);

        answerOneButton.setOnClickListener(this);
        answerTwoButton.setOnClickListener(this);
        answerThreeButton.setOnClickListener(this);
        answerFourButton.setOnClickListener(this);

        selectedQuizName = getIntent().getExtras().getString(Constants.QUIZ_NAME);
        indicator = getIntent().getExtras().getString(Constants.INDICATOR);
        userDetails = ((CustomApplication) getApplication()).getLoginUser();
        Log.d(TAG, selectedQuizName);
        if (Helper.isNetworkAvailable(this)) {
            if(indicator.equals(Constants.FROM_SIGNUP)){
                getAllQuizInGroupByNameWarmUp(selectedQuizName);
            }else{
                getAllQuizInGroupByName(selectedQuizName);
            }

        } else {
            displayErrorMessage(this, "No network available");
        }

    }

    private void displayCurrentQuestion() {

        if(!indicator.equals(Constants.FROM_SIGNUP)){
            deductCoin(currentQuestion.getCoins());
        }

        question_num.setText((questionNumber++)+"/"+ totalQuestion);

        if (question_image.getVisibility() == View.GONE) {
            question_image.setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(currentQuestion.getImage())) {
            String serverImagePath = Constants.PUBLIC_FOLDER + currentQuestion.getImage();
            Glide.with(QuizActivity.this).load(serverImagePath).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().override(250, 120).into(question_image);
        } else {
            question_image.setVisibility(View.GONE);
        }

        quiz_question.setText(currentQuestion.getQuestion());
        answerOneButton.setText(currentQuestion.getOption_one());
        answerTwoButton.setText(currentQuestion.getOption_two());
        answerThreeButton.setText(currentQuestion.getOption_three());
        answerFourButton.setText(currentQuestion.getOption_four());
        Log.d(TAG,"point"+currentQuestion.getPoint());
    }

    public void deductCoin(String coins){

        com.ssdtech.quizmaster.utils.Log.d(TAG, "getUserCoins");
        String URL = Constants.PATH_TO_DEDUCT_USER_COIN + "/" + userDetails.getMsisdn()+"/"+coins;
        GsonRequest<UserCoins> serverRequest = new GsonRequest<>(
                Request.Method.POST,
                URL,
                UserCoins.class,
                null,
                getUserCoinsSuccessListener(),
                createRequestErrorListener());
        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Response.Listener<UserCoins> getUserCoinsSuccessListener() {
        //com.ssdtech.quizmaster.utils.Log.d(TAG, "getUserCoins Response");
        return new Response.Listener<UserCoins>() {
            @Override
            public void onResponse(UserCoins response) {
                try {
                    if (response != null) {

                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Override
    public void onClick(View v) {
        Button clickedButton = (Button) v;

        String answerInText = clickedButton.getText().toString();
        int answerId = clickedButton.getId();

        int answerNumber = getButtonNumberClicked(answerId);

        int correctAnswer = Integer.parseInt(currentQuestion.getAnswer());
        String correctAnswerInText = correctAnswerInText(correctAnswer);

        if (countDownTimer != null) {
            counter = 0;
            timeProgress.setProgress(counter);
            countDownTimer.cancel();
        }

        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (answerNumber == correctAnswer) {
            point+= Long.parseLong(currentQuestion.getPoint());
            Log.d(TAG,"total : "+point+"perquestion : "+Long.parseLong(currentQuestion.getPoint()));
            tvPoint.setText(point+"");
            // got the answer correct
            if (isSoundActive) {
                playMusic.playSound(false, R.raw.correct);
            }

            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                clickedButton.setBackgroundDrawable( getResources().getDrawable(R.drawable.field_background_login) );
            } else {
                clickedButton.setBackground( getResources().getDrawable(R.drawable.field_background_login));
            }
            //clickedButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.field_background_login));
            disableOptionButtons();
            String qNumber = "" + questionIndex + 1;
            resultList.add(new ResultWrapper(qNumber, currentQuestion.getQuestion(), correctAnswerInText, answerInText, 1,""+timeCount2/1000+"."+timeCount2%1000,currentQuestion.getPoint()));
        } else {
            // failed the answer
            if (isSoundActive) {
                playMusic.playSound(false, R.raw.wrong);
            }
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                clickedButton.setBackgroundDrawable( getResources().getDrawable(R.drawable.background_red) );
            } else {
                clickedButton.setBackground( getResources().getDrawable(R.drawable.background_red));
            }
            //clickedButton.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            Button correctButton = getButtonIdWithPosition(correctAnswer);
            String qNumber = "" + questionIndex + 1;
            resultList.add(new ResultWrapper(qNumber, currentQuestion.getQuestion(), correctAnswerInText, answerInText, 0,""+timeCount2/1000+"."+timeCount2%1000,"0"));
            assert correctButton != null;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                correctButton.setBackgroundDrawable( getResources().getDrawable(R.drawable.field_background_login) );
            } else {
                correctButton.setBackground( getResources().getDrawable(R.drawable.field_background_login));
            }
            // correctButton.setBackgroundColor(getResources().getColor(R.color.colorGreen));
        }

        questionIndex++;
        Log.d(TAG,"Question index "+questionIndex);
        if (questionIndex >= totalQuestion) {
            // quiz over
            Log.d(TAG,"quize over");
            Runnable navigate = new Runnable() {
                @Override
                public void run() {
                    stopTimer();
                    stopTimer2();
                    String resultString = gson.toJson(resultList);
                    Intent resultIntent = new Intent(QuizActivity.this, ResultActivity.class);
                    resultIntent.putExtra(Constants.QUIZ_RESULT, resultString);
                    resultIntent.putExtra(Constants.INDICATOR, indicator);
                    resultIntent.putExtra(Constants.SUBCATEGORY, selectedQuizName);
                    startActivity(resultIntent);
                    finish();
                }
            };
            handler.postDelayed(navigate, 2000);
        } else {
            currentQuestion = questions.get(questionIndex);
            Runnable codeToRun = new Runnable() {
                @Override
                public void run() {
                    resetButtonBackgroundColors();
                    displayCurrentQuestion();
                    enableOptionButtons();
                    updateCounter();
                }

                ;
            };
            handler.postDelayed(codeToRun, 2000);
        }
    }

    private int getButtonNumberClicked(int buttonId) {
        int number = 0;
        switch (buttonId) {
            case R.id.answer_one:
                return 1;
            case R.id.answer_two:
                return 2;
            case R.id.answer_three:
                return 3;
            case R.id.answer_four:
                return 4;
        }
        return 0;
    }

    private String correctAnswerInText(int answer) {
        if (answer == 1) {
            return answerOneButton.getText().toString();
        }
        if (answer == 2) {
            return answerTwoButton.getText().toString();
        }
        if (answer == 3) {
            return answerThreeButton.getText().toString();
        }
        if (answer == 4) {
            return answerFourButton.getText().toString();
        }
        return "";
    }

    private Button getButtonIdWithPosition(int position) {
        if (position == 1) {
            return answerOneButton;
        } else if (position == 2) {
            return answerTwoButton;
        } else if (position == 3) {
            return answerThreeButton;
        } else if (position == 4) {
            return answerFourButton;
        }
        return null;
    }

    private void enableOptionButtons() {
        answerOneButton.setEnabled(true);
        answerTwoButton.setEnabled(true);
        answerThreeButton.setEnabled(true);
        answerFourButton.setEnabled(true);
    }

    private void disableOptionButtons() {
        answerOneButton.setEnabled(false);
        answerTwoButton.setEnabled(false);
        answerThreeButton.setEnabled(false);
        answerFourButton.setEnabled(false);
    }

    private void resetButtonBackgroundColors() {
        answerOneButton.setBackground( getResources().getDrawable(R.drawable.answer_background));
        answerTwoButton.setBackground( getResources().getDrawable(R.drawable.answer_background));
        answerThreeButton.setBackground( getResources().getDrawable(R.drawable.answer_background));
        answerFourButton.setBackground( getResources().getDrawable(R.drawable.answer_background));
        /*answerOneButton.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        answerTwoButton.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        answerThreeButton.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        answerFourButton.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));*/
    }

    private void getAllQuizInGroupByName(String name) {
        Map<String, String> params = getParams(name);
        Log.d(TAG, Constants.PATH_TO_QUIZ_QUESTION);
        Log.d(TAG,"allquestion"+ params.toString());
        GsonRequest<QuestionObject[]> serverRequest = new GsonRequest<QuestionObject[]>(
                Request.Method.POST,
                Constants.PATH_TO_QUIZ_QUESTION,
                QuestionObject[].class,
                params,
                createRequestSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private void getAllQuizInGroupByNameWarmUp(String name) {
        Map<String, String> params = getParams(name);
        Log.d(TAG, Constants.PATH_TO_QUIZ_QUESTION_WARMUP);
        Log.d(TAG, params.toString());
        GsonRequest<QuestionObject[]> serverRequest = new GsonRequest<QuestionObject[]>(
                Request.Method.POST,
                Constants.PATH_TO_QUIZ_QUESTION_WARMUP,
                QuestionObject[].class,
                params,
                createRequestSuccessListener(),
                createRequestErrorListener());
        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Map<String, String> getParams(String name) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.NAME, name);
        return params;
    }

    private Response.Listener<QuestionObject[]> createRequestSuccessListener() {
        return new Response.Listener<QuestionObject[]>() {
            @Override
            public void onResponse(QuestionObject[] response) {
                try {
                    if (response != null) {
                        questions = arrayToListObject(response);
                        Log.d(TAG,"questions: "+questions);
                        currentQuestion = questions.get(questionIndex);
                        questionNumber = questionIndex + 1;
                        totalQuestion = questions.size();
                        displayCurrentQuestion();
                        updateCounter();
                        startTimer();
                        startTimer2();
                    } else {
                        DisplayMessage.displayErrorMessage(QuizActivity.this, "No question found in this quiz ");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    private List<QuestionObject> arrayToListObject(QuestionObject[] response) {
        List<QuestionObject> allCategories = new ArrayList<>();
        Collections.addAll(allCategories, response);
        return allCategories;
    }

    @Override
    protected void onPause() {
        super.onPause();
        playAudio.stopSound();
        countDownTimer.cancel();
    }

    private void updateCounter() {
        countDownTimer = new CountDownTimer(21000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.d(TAG, "Counting time " + millisUntilFinished + " " + counter);
                counter++;
                timeProgress.setProgress(counter);
            }

            @Override
            public void onFinish() {
                playMusic.playSound(false, R.raw.wrong);
                int correctAnswer = Integer.parseInt(currentQuestion.getAnswer());
                Button answerButton = getButtonIdWithPosition(correctAnswer);
                String correctAnswerInText = correctAnswerInText(correctAnswer);

                assert answerButton != null;
                answerButton.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                disableOptionButtons();

                String qNumber = "" + questionIndex + 1;
                resultList.add(new ResultWrapper(qNumber, currentQuestion.getQuestion(), correctAnswerInText, "", 0,""+timeCount2/1000+"."+timeCount2%1000,"0"));
                counter = 0;
                questionIndex++;
                if (questionIndex >= totalQuestion) {
                    // quiz over
                    Runnable navigate = new Runnable() {
                        @Override
                        public void run() {
                            String resultString = gson.toJson(resultList);
                            Intent resultIntent = new Intent(QuizActivity.this, ResultActivity.class);
                            resultIntent.putExtra(Constants.QUIZ_RESULT, resultString);
                            resultIntent.putExtra(Constants.INDICATOR, indicator);
                            resultIntent.putExtra(Constants.SUBCATEGORY, selectedQuizName);
                            startActivity(resultIntent);
                            finish();
                        }
                    };
                    handler.postDelayed(navigate, 2000);
                } else {
                    currentQuestion = questions.get(questionIndex);
                    Runnable codeToRun = new Runnable() {
                        @Override
                        public void run() {
                            resetButtonBackgroundColors();
                            displayCurrentQuestion();
                            timeProgress.setProgress(counter);
                            enableOptionButtons();
                            updateCounter();
                        }
                    };
                    handler.postDelayed(codeToRun, 2000);
                }
            }
        };
        countDownTimer.start();
    }

    private void startTimer(){
        final NumberFormat nf = new DecimalFormat("00");
        T.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {

                        tv_time.setText(""+nf.format(timeCount/60)+":"+nf.format(timeCount%60));
                        timeCount++;
                    }
                });
            }
        }, 1000, 1000);

    }

    private void startTimer2(){
        final NumberFormat nf = new DecimalFormat("00");
        T.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        timeCount2++;
                    }
                });
            }
        }, 1, 1);

    }

    private void stopTimer2(){
        T2.cancel();
    }
    private void stopTimer(){
        T.cancel();
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //are you sure you want to exit quiz
        DisplayMessage.showMessageDialog(QuizActivity.this, "Are you sure you want to exit quiz?");

    }
}
