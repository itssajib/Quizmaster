package com.ssdtech.quizmaster;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.DisplayMessage;

public class WebActivity extends AppCompatActivity {

    private static final String TAG = WebActivity.class.getSimpleName();

    private WebView webView;
    private WebSettings webSettings;

    private String filename;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        webView = (WebView)findViewById(R.id.webview);

        webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDisplayZoomControls(true);

        webView.setWebViewClient(new CustomWebView());

        //prepare external url to load
        String remoteUrl = Constants.PUBLIC_API_FOLDER + getRequiredPage();
        Log.d(TAG, " Path " + remoteUrl);
        setTitle(filename);
        if(!remoteUrl.contains("http")){
            DisplayMessage.displayErrorMessage(this, "Invalid url");
        }else{
            webView.loadUrl(remoteUrl);
        }
    }

    private String getRequiredPage(){
        String intentValue = getIntent().getExtras().getString("SETTINGS");
        if(intentValue.equals("Terms")){
            filename = "Terms of usage";
            return "terms";
        }
        if(intentValue.equals("Policy")){
            filename = "Privacy Policy";
            return "policy";
        }
        if(intentValue.equals("License")){
            filename = "Third Party Licenses";
            return "license";
        }
        return "";
    }

    private class CustomWebView extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
