package com.ssdtech.quizmaster;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ssdtech.quizmaster.adapters.CategoryAdapter;
import com.ssdtech.quizmaster.entities.SingleQuizObject;
import com.ssdtech.quizmaster.entities.UserObject;
import com.ssdtech.quizmaster.network.GsonRequest;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.DisplayMessage;
import com.ssdtech.quizmaster.utils.Helper;
import com.ssdtech.quizmaster.utils.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ssdtech.quizmaster.utils.DisplayMessage.displayErrorMessage;

public class SelectQuizActivity extends AppCompatActivity {
    
    private static final String TAG = SelectQuizActivity.class.getSimpleName();

    private int categoryId;
    private String catName = "SSD";
    private String catImage;

    private ImageView categoryImage;
    private TextView categoryName;

    private View backbtn,social_share;

    private RecyclerView quizRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_select_quiz);

        setContentView(R.layout.sekect_category_activity);
        getSupportActionBar().hide();
       // categoryId = getIntent().getExtras().getInt(Constants.CATEGORY);
       // catName = getIntent().getExtras().getString(Constants.CATEGORY_NAME);
        //catImage = getIntent().getExtras().getString(Constants.CATEGORY_IMAGE);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if(getSupportActionBar() != null){
            getSupportActionBar().setElevation(0);
        }

        setTitle("");

        categoryImage = (ImageView)findViewById(R.id.quiz_category_image);
        categoryName = (TextView)findViewById(R.id.quiz_category_name);
        social_share=findViewById(R.id.social_share);

        quizRecyclerView = (RecyclerView)findViewById(R.id.quiz_category);
        quizRecyclerView.setLayoutManager(new GridLayoutManager(SelectQuizActivity.this, 1));
        quizRecyclerView.setHasFixedSize(true);

        if(Helper.isNetworkAvailable(this)){
            allSubcategoryInCategory(catName);
        }else{
            DisplayMessage.displayErrorMessage(this, "No network available");
        }

        social_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.ssdtech.quizmaster&hl=en");
                startActivity(Intent.createChooser(intent, "Share"));
            }
        });

        backbtn = findViewById(R.id.backbtn);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void allSubcategoryInCategory(String name){
        Map params = getParams(name);
        GsonRequest<SingleQuizObject[]> serverRequest = new GsonRequest<SingleQuizObject[]>(
                Request.Method.POST,
                Constants.PATH_TO_QUIZ_CATEGORIES,
                SingleQuizObject[].class,
                params,
                createRequestSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication)getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Map getParams(String name){
        Map<String, String> params = new HashMap<String,String>();
        params.put(Constants.NAME, name);
        Log.d(TAG,"category : "+name);
        return params;
    }

    private Response.Listener<SingleQuizObject[]> createRequestSuccessListener() {
        return new Response.Listener<SingleQuizObject[]>() {
            @Override
            public void onResponse(SingleQuizObject[] response) {
                try {
                    if(response != null){
                        categoryName.setText(catName + " subcategories");
                        String serverImagePath = Constants.PUBLIC_FOLDER + catImage;
                        Glide.with(SelectQuizActivity.this).load(serverImagePath).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().override(80, 80).into(categoryImage);

                        CategoryAdapter mAdapter = new CategoryAdapter(SelectQuizActivity.this, arrayToListObject(response));
                        quizRecyclerView.setAdapter(mAdapter);

                    } else{
                        displayErrorMessage(SelectQuizActivity.this, "No subcategory found ");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    private List<SingleQuizObject> arrayToListObject(SingleQuizObject[] response){
        List<SingleQuizObject> allCategories = new ArrayList<>();
        Collections.addAll(allCategories, response);
        return allCategories;
    }

}
