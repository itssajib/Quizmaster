package com.ssdtech.quizmaster;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ssdtech.quizmaster.entities.UserObject;
import com.ssdtech.quizmaster.network.GsonRequest;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.Log;

import static com.ssdtech.quizmaster.utils.DisplayMessage.displayErrorMessage;

public class LoadingActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private NetworkStateReceiver networkStateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));


        //((CustomApplication) getApplication()).redirectLoginUsers();
        if(isOnline()){
            getMSISDN();
        }else{
            Toast.makeText(getApplicationContext(),getString(R.string.need_robi_network),Toast.LENGTH_LONG).show();
        }
    }

    private void getMSISDN() {
        Log.d(TAG, "getting MSISDN......");
        GsonRequest<UserObject> serverRequest = new GsonRequest<UserObject>(
                Request.Method.POST,
                Constants.PATH_TO_GRAB_MSISDN,
                UserObject.class,
                createRequestSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Response.Listener<UserObject> createRequestSuccessListener() {
        return new Response.Listener<UserObject>() {
            @Override
            public void onResponse(UserObject response) {
                try {
                    Log.d(TAG, "getting MSISDN : " + response);
                    if (response != null) {

                        if (response.getMsisdn() != null && !response.getMsisdn().equals("") && !response.getMsisdn().equals("null") && response.getMsisdn().length() > 9) {

                            if (response.getUsername() == null || response.getUsername().equals("") || response.getUsername().equals("null")) {
                                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                                intent.putExtra(Constants.MSISDN, response.getMsisdn());
                                startActivity(intent);
                                finish();

                            } else {
                                //save user login data to a shared preference
                                String userData = ((CustomApplication) getApplication()).getGsonObject().toJson(response);
                                ((CustomApplication) getApplication()).getShared().setUserData(userData);
                                ((CustomApplication) getApplication()).redirectLoginUsers();
                                finish();
                            }

                        } else {
                            displayErrorMessage(LoadingActivity.this, getString(R.string.need_robi_network));
                        }

                    } else {
                        Log.d(TAG, "getting MSISDN : null");
                        displayErrorMessage(LoadingActivity.this, getString(R.string.need_robi_network));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void networkAvailable() {
       // startActivity(new Intent(this,LoadingActivity.class));
        getMSISDN();
    }

    @Override
    public void networkUnavailable() {

    }
}
