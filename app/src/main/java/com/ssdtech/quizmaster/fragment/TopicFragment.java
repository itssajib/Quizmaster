package com.ssdtech.quizmaster.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ssdtech.quizmaster.R;
import com.ssdtech.quizmaster.adapters.QuizAdapter;
import com.ssdtech.quizmaster.entities.CategoryObject;
import com.ssdtech.quizmaster.network.GsonRequest;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.DisplayMessage;
import com.ssdtech.quizmaster.utils.Helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.ssdtech.quizmaster.utils.DisplayMessage.displayErrorMessage;


public class TopicFragment extends Fragment {

    private static final String TAG = TopicFragment.class.getSimpleName();

    private RecyclerView selectRecyclerView;

    public TopicFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setElevation(0);
        getActivity().setTitle("Main Category");

        View view = inflater.inflate(R.layout.fragment_topic, container, false);

        selectRecyclerView = (RecyclerView)view.findViewById(R.id.select_quiz);
        selectRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        selectRecyclerView.setHasFixedSize(true);

        if(Helper.isNetworkAvailable(getActivity())){
            fetchQuizCategories();
        }else{
            DisplayMessage.displayErrorMessage(getActivity(), "No network available");
        }

        return view;
    }

    private void fetchQuizCategories(){
        GsonRequest<CategoryObject[]> serverRequest = new GsonRequest<CategoryObject[]>(
                Request.Method.GET,
                Constants.PATH_TO_QUIZ_CATEGORIES,
                CategoryObject[].class,
                createRequestSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication)getActivity().getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Response.Listener<CategoryObject[]> createRequestSuccessListener() {
        return new Response.Listener<CategoryObject[]>() {
            @Override
            public void onResponse(CategoryObject[] response) {
                try {
                    if(response != null){
                        QuizAdapter mAdapter = new QuizAdapter(getActivity(), arrayToListObject(response));
                        selectRecyclerView.setAdapter(mAdapter);

                    } else{
                        displayErrorMessage(getActivity(), "No quiz category has been added");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    private List<CategoryObject> arrayToListObject(CategoryObject[] response){
        List<CategoryObject> allCategories = new ArrayList<>();
        Collections.addAll(allCategories, response);
        return allCategories;
    }

}
