package com.ssdtech.quizmaster.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.ssdtech.quizmaster.R;
import com.ssdtech.quizmaster.WebActivity;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.CustomSpinnerAdapter;
import com.ssdtech.quizmaster.utils.Helper;

import java.util.ArrayList;

import static com.ssdtech.quizmaster.R.id.music;
import static com.ssdtech.quizmaster.R.id.sound;


public class SettingsFragment extends Fragment {

    private static final String TAG = SettingsFragment.class.getSimpleName();

    private SwitchCompat soundSwitch, musicSwitch;

    private Spinner languageSpinner;

    private TextView termsOfUsage, privacyPolicy, thirdPartyLicense, rateTheApp;

    private boolean isSoundOn;
    private boolean isMusicOn;


    public SettingsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        getActivity().setTitle("Settings");

        soundSwitch = (SwitchCompat)view.findViewById(sound);
        boolean soundState = ((CustomApplication)getActivity().getApplication()).getShared().getSavedSound();
        if(soundState){
            soundSwitch.setChecked(true);
        }
        else{
            soundSwitch.setChecked(false);
        }
        soundSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isSoundOn = isChecked;
                if(isChecked){
                    ((CustomApplication)getActivity().getApplication()).getShared().saveSound(true);
                }else{
                    ((CustomApplication)getActivity().getApplication()).getShared().saveSound(false);
                }
            }
        });

        musicSwitch = (SwitchCompat)view.findViewById(music);
        boolean musicState = ((CustomApplication)getActivity().getApplication()).getShared().getSavedMusic();
        if(musicState){
           musicSwitch.setChecked(true);
        }
        else{
            musicSwitch.setChecked(false);
        }
        musicSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isMusicOn = isChecked;
                if(isChecked){
                    ((CustomApplication)getActivity().getApplication()).getShared().saveMusic(true);
                }else{
                    ((CustomApplication)getActivity().getApplication()).getShared().saveMusic(false);
                }
            }
        });

        languageSpinner = (Spinner)view.findViewById(R.id.language);
        ArrayList<String> languages = new ArrayList<>();
        languages.add("English");
        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(getActivity(),languages);
        languageSpinner.setAdapter(customSpinnerAdapter);
        languageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                ((CustomApplication)getActivity().getApplication()).getShared().saveLanguage(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        termsOfUsage = (TextView)view.findViewById(R.id.terms_of_usage);
        termsOfUsage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), WebActivity.class);
                intent.putExtra("SETTINGS", "Terms");
                startActivity(intent);
            }
        });

        privacyPolicy = (TextView)view.findViewById(R.id.privacy_policy);
        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), WebActivity.class);
                intent.putExtra("SETTINGS", "Policy");
                startActivity(intent);
            }
        });

        thirdPartyLicense = (TextView)view.findViewById(R.id.third_party_licenses);
        thirdPartyLicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), WebActivity.class);
                intent.putExtra("SETTINGS", "License");
                startActivity(intent);
            }
        });

        rateTheApp = (TextView)view.findViewById(R.id.rate_the_app);
        rateTheApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.rateApp(getActivity());
            }
        });

        return view;
    }

}
