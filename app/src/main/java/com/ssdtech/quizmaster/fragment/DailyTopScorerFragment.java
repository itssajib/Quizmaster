package com.ssdtech.quizmaster.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ssdtech.quizmaster.R;
import com.ssdtech.quizmaster.adapters.LadderBoardAdapter;
import com.ssdtech.quizmaster.entities.TopScoreObject;
import com.ssdtech.quizmaster.network.GsonRequest;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.DisplayMessage;
import com.ssdtech.quizmaster.utils.Helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class DailyTopScorerFragment extends Fragment {

    private static String TAG = DailyTopScorerFragment.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    public DailyTopScorerFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_daily_top_scorer, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(getActivity(), 3);
        mRecyclerView.setLayoutManager(mLayoutManager);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL);
        mRecyclerView.addItemDecoration(itemDecoration);

        if(Helper.isNetworkAvailable(getActivity())){
            getAllQuizInGroupByName();
        }else{
            DisplayMessage.displayErrorMessage(getActivity(), "No network available");
        }

        return view;
    }

    private void getAllQuizInGroupByName(){
        GsonRequest<TopScoreObject[]> serverRequest = new GsonRequest<TopScoreObject[]>(
                Request.Method.GET,
                Constants.PATH_TO_DAILY_SCORE,
                TopScoreObject[].class,
                createRequestSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication)getActivity().getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Response.Listener<TopScoreObject[]> createRequestSuccessListener() {
        return new Response.Listener<TopScoreObject[]>() {
            @Override
            public void onResponse(TopScoreObject[] response) {
                try {
                    if(response != null){
                        List<TopScoreObject> mList = arrayToListObject(response);
                        mAdapter = new LadderBoardAdapter(mList);
                        mRecyclerView.setAdapter(mAdapter);
                    } else{
                        DisplayMessage.displayErrorMessage(getActivity(), "No score found yet");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    private List<TopScoreObject> arrayToListObject(TopScoreObject[] response){
        List<TopScoreObject> allCategories = new ArrayList<>();
        Collections.addAll(allCategories, response);
        return allCategories;
    }

}
