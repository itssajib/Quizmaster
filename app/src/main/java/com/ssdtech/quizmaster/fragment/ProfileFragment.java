package com.ssdtech.quizmaster.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.ssdtech.quizmaster.R;
import com.ssdtech.quizmaster.adapters.MyQuizAdapter;
import com.ssdtech.quizmaster.adapters.PerformanceAdapter;
import com.ssdtech.quizmaster.entities.CheckObject;
import com.ssdtech.quizmaster.entities.PerformanceObject;
import com.ssdtech.quizmaster.entities.ProfileObject;
import com.ssdtech.quizmaster.entities.QuizObject;
import com.ssdtech.quizmaster.entities.UserObject;
import com.ssdtech.quizmaster.network.GsonRequest;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.DisplayMessage;
import com.ssdtech.quizmaster.utils.Helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.ssdtech.quizmaster.utils.DisplayMessage.displayErrorMessage;

public class ProfileFragment extends Fragment {

    private static final String TAG = ProfileFragment.class.getSimpleName();

    private UserObject loginUser;

    private TextView playerStatus, playerScore, followedQuiz;

    private RecyclerView selectedQuizRecyclerView;

    private RecyclerView recyclerPerformance;


    public ProfileFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        getActivity().setTitle("Home");

        loginUser = ((CustomApplication)getActivity().getApplication()).getLoginUser();

        playerStatus = (TextView)view.findViewById(R.id.player_status);
        playerScore = (TextView)view.findViewById(R.id.player_score);

        followedQuiz = (TextView)view.findViewById(R.id.followed_quiz);

        TextView playerName = (TextView)view.findViewById(R.id.player_name);
        playerName.setText(loginUser.getNames());

        if(Helper.isNetworkAvailable(getActivity())){
            userProfileScores(loginUser.getEmail());
        }else{
            DisplayMessage.displayErrorMessage(getActivity(), "No network available");
        }

        selectedQuizRecyclerView = (RecyclerView)view.findViewById(R.id.selected_quizzes);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        selectedQuizRecyclerView.setLayoutManager(gridLayoutManager);
        selectedQuizRecyclerView.setHasFixedSize(true);

        recyclerPerformance = (RecyclerView)view.findViewById(R.id.performance_list);
        recyclerPerformance.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerPerformance.setHasFixedSize(true);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        List<CheckObject> followedData = allFollowedQuizzes();
        if(followedData.size() <= 0){
            followedQuiz.setVisibility(View.VISIBLE);
            selectedQuizRecyclerView.setVisibility(View.GONE);
        }else{
            followedQuiz.setVisibility(View.GONE);
            selectedQuizRecyclerView.setVisibility(View.VISIBLE);

            MyQuizAdapter mAdapter = new MyQuizAdapter(getActivity(), followedData, R.layout.selected_quiz_layout);
            selectedQuizRecyclerView.setAdapter(mAdapter);
        }
    }

    private List<CheckObject> allFollowedQuizzes(){
        final Gson gson = ((CustomApplication)getActivity().getApplication()).getGsonObject();
        String checkList = ((CustomApplication)getActivity().getApplication()).getShared().getCheckQuiz();
        List<CheckObject> selectedQuiz = null;
        if(!TextUtils.isEmpty(checkList)){
            CheckObject[] checkObjects = gson.fromJson(checkList, CheckObject[].class);
            selectedQuiz = new LinkedList<>(Arrays.asList(checkObjects));
        }else{
            selectedQuiz = new ArrayList<>();
        }
        return selectedQuiz;
    }

    public List<QuizObject> getTestData() {
        List<QuizObject> testData = new ArrayList<>();
        testData.add(new QuizObject("", "General Knowledge"));
        testData.add(new QuizObject("", "Entertainment"));
        testData.add(new QuizObject("", "History"));
        testData.add(new QuizObject("", "Sports"));
        return testData;
    }

    private void userProfileScores(String name){
        Map params = getParams(name);
        GsonRequest<ProfileObject> serverRequest = new GsonRequest<ProfileObject>(
                Request.Method.POST,
                Constants.PATH_TO_USER_PROFILE,
                ProfileObject.class,
                params,
                createRequestSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication)getActivity().getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Map getParams(String email){
        Map<String, String> params = new HashMap<String,String>();
        params.put(Constants.EMAIL, email);
        return params;
    }

    private Response.Listener<ProfileObject> createRequestSuccessListener() {
        return new Response.Listener<ProfileObject>() {
            @Override
            public void onResponse(ProfileObject response) {
                try {
                    if(!TextUtils.isEmpty(response.getProfile())){
                        ProfileObject profileObject = (ProfileObject)response;
                        List<PerformanceObject> returnedScores = stringToArray(profileObject.getProfile());

                        PerformanceObject playerLevel = returnedScores.get(0);
                        playerScore.setText(playerLevel.getValue());
                        playerStatus.setText(quizPlayerLevel(playerLevel.getValue()));

                        returnedScores.remove(0);
                        PerformanceAdapter mPerformance = new PerformanceAdapter(getActivity(), returnedScores);
                        recyclerPerformance.setAdapter(mPerformance);


                    } else{
                        displayErrorMessage(getActivity(), "You are not following any quiz");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    private List<PerformanceObject> stringToArray(String resultArray){
        String prettyString = resultArray.substring(0, resultArray.length()-1);
        String[] userDetails = prettyString.split(";");

        List<PerformanceObject> quizDetails = new ArrayList<>();

        for(int i = 0; i < userDetails.length; i++){
            String contentValue = userDetails[i];
            String[] contentArray = contentValue.split(":");
            quizDetails.add(new PerformanceObject(contentArray[0], contentArray[1]));
        }
        return quizDetails;
    }

    private String quizPlayerLevel(String playerCount){
        if(Integer.parseInt(playerCount) >= 10000){
            return "Advance";
        }
        if(Integer.parseInt(playerCount) >= 10000){
           return "Intermediate";
        }
        return "Beginner";
    }

}
