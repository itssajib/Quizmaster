package com.ssdtech.quizmaster.utils;

/**
 * Created by Nazibul on 5/21/2017.
 */

public class Log {

    public static boolean logEnable = true;

    public static void d(String tag, String logmsg) {
        if (logEnable) {
            android.util.Log.d(tag, logmsg);
        }
    }

    public static void e(String tag, String logmsg) {
        if (logEnable) {
            android.util.Log.e(tag, logmsg);
        }
    }

    public static void i(String tag, String logmsg) {
        if (logEnable) {
            android.util.Log.i(tag, logmsg);
        }
    }

    public static void v(String tag, String logmsg) {
        if (logEnable) {
            android.util.Log.v(tag, logmsg);
        }
    }
}
