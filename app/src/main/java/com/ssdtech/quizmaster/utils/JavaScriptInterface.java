package com.ssdtech.quizmaster.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.ssdtech.quizmaster.interfaces.ActivityInteractionListener;

/**
 * Created by Moyeenur ohhn 10/15/2015.
 */

public class JavaScriptInterface {
    String TAG = "JavaScript__Log";
    Context mContext;
    ProgressDialog dialog;
    ActivityInteractionListener aiListner;
    Handler handler;

    /**
     * Instantiate the interface and set the context
     */
    public JavaScriptInterface(Context c, ActivityInteractionListener ail) {
        mContext = c;
        dialog = new ProgressDialog(mContext);
        handler = new Handler();
        aiListner=ail;
    }

    @JavascriptInterface
    public void showToast(String message) {
        Toast.makeText(mContext, message + "", Toast.LENGTH_LONG).show();
    }

    @JavascriptInterface
    public void refreshPage(){

        aiListner.onRefresh();

    }
}

