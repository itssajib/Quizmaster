package com.ssdtech.quizmaster.utils;

import android.net.Uri;
import android.view.View;
import android.widget.ImageButton;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Nazibul on 1/25/2017.
 */

public interface OnMainActivityFragmentListener {

    void onActivityUpdateUI(Runnable runnable);

    void onCallFragmentInteraction(JSONObject item, String indicator);

    void onBlockedFragmentInteraction(JSONObject item, String indicator);

    void onCallHistoryFragmentInteraction(JSONObject item, String indicator);

    void onFollowerFragmentInteraction(JSONObject item, String indicator);

    void onFollowingFragmentInteraction(JSONObject item, String indicator);

    void onHomeFragmentInteraction(JSONObject item, String indicator);

    void onProfileEditFragmentInteraction(String indicator, String value);

    void onProfileViewFragmentInteraction(String indicator, JSONObject item);

    void onSettingsFragmentInteraction(Uri uri);

    void onUserDetailFragmentInteraction(JSONObject item, String indicator);

    void onVoiceRecordFragmentInteraction(String indicator, ImageButton saveBtn, ImageButton playButton);

    void onLoginFragmentInteraction(String indicator);

    void onPackageFragmentInteraction(String indicator, JSONObject data);

    void onRegistrationFragmentInteraction(String indicator);

    void onInterestFragmentInteraction(JSONObject item, String indicator);

    void onSearchFragmentInteraction(String indicator, JSONObject obj);

    void onAnimationPlay(String animationStart, View view, String direction, int distance, int time);

    void onHomeFragmentTabInteraction(String indicator, String animationDirection);

    void updateApplicationUI(Runnable updateRunnable);

    void nearbyFragmentInterection();

    void onIMHistoryFragmentInteraction(JSONObject item, String indicator);

    void onHomeFragmentTabInteraction(String tabHomePressed);

    void onCommonListInteraction(JSONObject mItem, String profilePicture);

    void showHideLoader(boolean show, boolean cancellabe);

    void customToast(String message, int time);

    void onTopupFragmentInteraction(String indicator);

    void onNotificationFragmentInteraction(JSONArray item, String indicator);

    void onAddfriendListener(JSONObject mItem);

    void onFilterFragmenyInteraction(String indicator, JSONObject value);

    void webviewPopupLoadContent(String sb);

    void webviewPopupJavascriptInterface(String indicator, String info);
}
