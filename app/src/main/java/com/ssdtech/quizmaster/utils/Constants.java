package com.ssdtech.quizmaster.utils;


public class Constants {

    public static final String ID = "id";
    public static final String NAMES = "names";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String MSISDN = "msisdn";
    public static final String USERNAME = "user_name";
    public static final String IMAGEPATH = "mood_avatar";

    public static final String CATEGORY = "category";
    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_IMAGE = "category_image";
    public static final String SUBCATEGORY = "subcategory";
    public static final String SCORE = "score";
    public static final String POINT = "point";
    public static final String TIME = "time";
    public static final String USERID = "userid";
    public static final String INDICATOR="indicator";
    public static final String FROM_QUIZE_DETAIL="FROM_QUIZE_DETAIL";
    public static final String FROM_WARM_UP="FROM_WARM_UP";
    public static final String FROM_SIGNUP="FROM_SIGNUP";
    public static final String FROM_EDITE="FROM_EDITE";
    public static final String FROM_WELCOME="FROM_WELCOME";
    public static final String FROM_QUIZEDETAIL="FROM_QUIZEDETAIL";

    public static final String CHECK_QUIZ = "CHECK_QUIZ";
    public static final String FOLLOW_QUIZ = "FOLLOW_QUIZ";
    public static final String QUIZ_NAME = "quiz_name";
    public static final String QUIZ_RESULT = "quiz_result";
    public static final String CURRENT_QUIZ = "current_quiz";
    public static final String MYPREF = "mypref";
    public static final String SCOREOBJECT = "scoreobject";
    public static final String DASHBOARD = "dashboard";
    public static final int DEDUCTION_AMOUNT = 1;


    public static final String SOUND = "SOUND";
    public static final String MUSIC = "MUSIC";
    public static final String LANGUAGE = "LANGUAGE";


    public static final int MY_SOCKET_TIMEOUT_MS = 20000;

    public static final String USER_DATA = "USER_DATA";
    public static final String SHARED_PREF = "SHARED_PREFERENCE";


    public static final String PUBLIC_FOLDER = "http://ssl.boomtalk.me/onlinequiz/public/";
    public static final String PUBLIC_API_FOLDER = "http://ssl.boomtalk.me/onlinequiz/public/api/";

    public static final String PATH_TO_GRAB_MSISDN = PUBLIC_API_FOLDER + "grabmsisdn";
    public static final String PATH_TO_SERVER_LOGIN = PUBLIC_API_FOLDER + "login";
    public static final String PATH_TO_SERVER_SIGN_IN = PUBLIC_API_FOLDER + "signin";
    public static final String PATH_TO_SERVER_RESET_PASSWORD = PUBLIC_API_FOLDER + "passwordreset";
    public static final String PATH_TO_QUIZ_CATEGORIES = PUBLIC_API_FOLDER + "categories";
    public static final String PATH_TO_QUIZ_SUBCATEGORIES = PUBLIC_API_FOLDER + "subcategories";
    public static final String PATH_TO_SINGLE_QUIZ_SCORE = PUBLIC_API_FOLDER + "singlescore";
    public static final String PATH_TO_QUIZ_QUESTION = PUBLIC_API_FOLDER + "quizquestion";
    public static final String PATH_TO_QUIZ_QUESTION_WARMUP = PUBLIC_API_FOLDER + "Dummyquizquestion";
    public static final String PATH_TO_USER_PROFILE = PUBLIC_API_FOLDER + "userprofile";
    public static final String PATH_TO_BEST_SCORE = PUBLIC_API_FOLDER + "allscores";
    public static final String PATH_TO_DAILY_SCORE = PUBLIC_API_FOLDER + "dailyscores";
    public static final String PATH_TO_ADD_SCORE = PUBLIC_API_FOLDER + "addscore";
    public static final String PATH_TO_HOME_WEBVIEW = PUBLIC_API_FOLDER + "todaysgift";
    public static final String PATH_TO_HISTORY_WEBVIEW = PUBLIC_API_FOLDER + "history-score";
    public static final String PATH_TO_LEADERBOARD_WEBVIEW = PUBLIC_API_FOLDER + "leaderboard";
    public static final String PATH_TO_FAQ_WEBVIEW = PUBLIC_API_FOLDER + "faq";
    public static final String PATH_TO_QUIZ_DETAILS = PUBLIC_API_FOLDER + "getQuizDetails";
    public static final String PATH_TO_BUY_COIN = PUBLIC_API_FOLDER + "buycoin";
    public static final String PATH_TO_GET_USER_COIN = PUBLIC_API_FOLDER + "getUserCoin";
    public static final String PATH_TO_GET_USER_POSITION = PUBLIC_API_FOLDER + "userposition";
    public static final String PATH_TO_DEDUCT_USER_COIN = PUBLIC_API_FOLDER + "deductUserCoin";
    public static final String PATH_TO_GET_MOOD_AVATAR = PUBLIC_API_FOLDER + "getMoodAvatar";
    public static final String PATH_TO_SAVE_MOOD_AVATAR = PUBLIC_API_FOLDER + "saveMoodAvatar";

}
