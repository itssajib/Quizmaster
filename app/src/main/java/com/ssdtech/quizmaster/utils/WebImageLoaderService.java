package com.ssdtech.quizmaster.utils;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.URL;

public class WebImageLoaderService extends IntentService {

    private static String TAG = "WebImageLoder";
    private static final String EXTRA_URL = "EXTRA_URL";
    private static final String EXTRA_MESSANGER = "EXTRA_MESSANGER";
    private static final String IMG_DATA = "IMG_DATA";

    public WebImageLoaderService() {
        super("WebImageLoaderService");
    }

    public static void loadWebImage(final Context context, final ImageView imageView, String url) {
        Log.d(TAG, "Loading image from : " + url);
        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Bundle reply = msg.getData();
                byte[] img = reply.getByteArray(IMG_DATA);
                Log.d(TAG, "displaying image length : " + img.length);
                Bitmap bitmap = null;
                try {
                    bitmap = BitmapFactory.decodeByteArray(img, 0, img.length);
                    if (bitmap != null) {
                        imageView.setImageBitmap(bitmap);
                        Log.d(TAG,"bitmap not null");
                    }else{
                        Log.d(TAG,"bitmap is null");
                    }
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                }
            }
        };

        Intent intent = new Intent(context, WebImageLoaderService.class);
        intent.putExtra(EXTRA_MESSANGER, new Messenger(handler));
        intent.putExtra(EXTRA_URL, url);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String url = intent.getStringExtra(EXTRA_URL);

            try {
                byte[] img = ImageCacheDB.retrieveKey(getApplication(), url);
                if (img == null) {
                   // Log.d(TAG, "imgCache null");
                    final Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                    img = baos.toByteArray();
                    //Log.d(TAG, "imgCache insertByte : " + img.length);
                    boolean insertid = ImageCacheDB.insertOrUpdate(getApplicationContext(), url, img);
                    //Log.d(TAG, "imgCache insert : " + insertid);
                } else {
                    //Log.d(TAG, "Loading image from DB Cache : " + img.length);
                }

                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    Messenger messenger = (Messenger) bundle.get(EXTRA_MESSANGER);
                    Message msg = Message.obtain();
                    Bundle data = new Bundle();
                    data.putByteArray(IMG_DATA, img);
                    msg.setData(data); //put the data here
                    try {
                        messenger.send(msg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (ConnectException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static class ImageCacheDB {

        private static final String DATABASE_NAME = "QuizImageCache";
        private static final int DATABASE_VERSION = 1;
        private static final String TABLE_NAME = "`Cacheimage`";
        private static final String KEY_COLUMN = "`key`";
        private static final String VALUE_COLUMN = "`value`";
        private static ImageCacheDB.DBHelper helper = null;

        private static boolean createAllTables(Context context) {
            if (helper == null)
                helper = new ImageCacheDB.DBHelper(context);
            return true;
        }

        private static boolean insertOrUpdate(Context context, String key, byte[] blob) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_COLUMN, key);
            contentValues.put(VALUE_COLUMN, blob);

            if (helper == null)
                helper = new ImageCacheDB.DBHelper(context);
            long rowId = helper.getWritableDatabase().insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
            return (rowId > -1);
        }

        private static byte[] retrieveKey(Context context, String key) {
            byte[] result = null;

            String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_COLUMN + " = '" + key + "'";

            if (helper == null)
                helper = new ImageCacheDB.DBHelper(context);
            Cursor cursor = helper.getReadableDatabase().rawQuery(query, null);
            if (cursor.moveToFirst()) {
                return cursor.getBlob(1);
            }
            cursor.close();

            return result;
        }

        private static class DBHelper extends SQLiteOpenHelper {
            DBHelper(Context context) {
                super(context, DATABASE_NAME, null, DATABASE_VERSION);
            }

            @Override
            public void onCreate(SQLiteDatabase db) {
                db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + KEY_COLUMN + " TEXT primary key, " + VALUE_COLUMN + " BLOB)");
                //Log.d("ImageDB__Log:", "table created");
            }

            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
                db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + KEY_COLUMN + " TEXT primary key, " + VALUE_COLUMN + " BLOB)");
               // Log.d("ImageDB__Log:", "table upgraded");
            }
        }
    }
}
