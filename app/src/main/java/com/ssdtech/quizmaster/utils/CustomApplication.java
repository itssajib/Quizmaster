package com.ssdtech.quizmaster.utils;

import android.app.Application;
import android.content.Intent;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ssdtech.quizmaster.LoginActivity;
import com.ssdtech.quizmaster.MainActivityHome;
import com.ssdtech.quizmaster.entities.UserObject;
import com.ssdtech.quizmaster.network.NetworkCall;


public class CustomApplication extends Application {

    private Gson gson;
    private GsonBuilder builder;

    private CustomSharedPreference shared;

    private NetworkCall networkCall;

    @Override
    public void onCreate() {
        super.onCreate();
        builder = new GsonBuilder();
        gson = builder.create();
        shared = new CustomSharedPreference(getApplicationContext());
        networkCall = new NetworkCall(getApplicationContext());
    }

    public CustomSharedPreference getShared(){
        return shared;
    }

    public Gson getGsonObject(){
        return gson;
    }

    public NetworkCall getNetworkCall(){
        return networkCall;
    }

    /**
     * get current login user
     * @return
     */
    public UserObject getLoginUser(){
        Gson mGson = getGsonObject();
        String storedUser = getShared().getUserData();
        return mGson.fromJson(storedUser, UserObject.class);
    }

    public String getLoginUserString(){
        Gson mGson = getGsonObject();
        String storedUser = getShared().getUserData();
        return storedUser;
    }

    public boolean isUserLogin(){
        Gson mGson = getGsonObject();
        String storedUser = getShared().getUserData();
        if(TextUtils.isEmpty(storedUser)){
           return false;
        }
        return true;
    }

    public void checkUserLogin(){
        if(!isUserLogin()){
            Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(loginIntent);
        }
    }

    public void redirectLoginUsers(){
        if(isUserLogin()){
            Intent loginIntent = new Intent(getApplicationContext(), MainActivityHome.class);
            loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(loginIntent);
        }
    }
}