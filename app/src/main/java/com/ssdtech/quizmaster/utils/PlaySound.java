package com.ssdtech.quizmaster.utils;


import android.content.Context;
import android.media.MediaPlayer;

public class PlaySound {

    private Context context;

    private MediaPlayer mediaPlayer;

    public PlaySound(Context context){
        this.context = context;
    }

    public void playSound(boolean isMusic, int soundId){
        mediaPlayer = MediaPlayer.create(context, soundId);
        if(isMusic){
            mediaPlayer.setLooping(true);
        }
        mediaPlayer.start();
    }

    public void stopSound(){
        if(mediaPlayer.isPlaying() || mediaPlayer != null){
            mediaPlayer.pause();
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
}
