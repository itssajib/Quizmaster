package com.ssdtech.quizmaster.utils;

import android.content.Context;
import android.media.MediaPlayer;


public class PlayAudio {

    private Context context;

    private MediaPlayer player;

    public PlayAudio(Context context){
        this.context = context;
    }

    public void playSound(int soundId){
        player = MediaPlayer.create(context, soundId);
        player.setLooping(true);
        player.start();
    }


    public void stopSound(){
        if(player != null){
            if(player.isPlaying()){
                player.pause();
                player.stop();
                player.release();
                player = null;
            }
        }
    }
}
