package com.ssdtech.quizmaster.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ssdtech.quizmaster.ResultActivity;
import com.ssdtech.quizmaster.entities.ScoreObject;
import com.ssdtech.quizmaster.network.GsonRequest;

import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.ssdtech.quizmaster.utils.DisplayMessage.displayErrorMessage;

/**
 * Created by Sajib on 1/8/2018.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo wifi = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        final android.net.NetworkInfo mobile = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi.isAvailable() || mobile.isAvailable()) {
            // Do something
            context.sendBroadcast(new Intent("INTERNET_FOUND"));
            Log.d("Network Available ", "Flag No 1");
        }

    }

    /*private void sendScoreToServer(String subcategory, String username, String email, String score,String msisdn,String point,String time,String id) {
        Map<String, String> params = getParams(subcategory, username, email, score,msisdn,point,time,id);
        Log.d(TAG, "sendvalue "+params.toString() );
        GsonRequest<ScoreObject> serverRequest = new GsonRequest<ScoreObject>(
                Request.Method.POST,
                Constants.PATH_TO_ADD_SCORE,
                ScoreObject.class,
                params,
                createRequestSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication) ).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Map<String, String> getParams(String subcategory, String username, String email, String score,String msisdn,String point,String time,String id) {
        Map<String, String> params = new HashMap<String, String>();
        //params.put(Constants.SUBCATEGORY, subcategory);
        params.put(Constants.NAME, username);
        params.put(Constants.EMAIL, email);
        params.put(Constants.SCORE, score);
        params.put(Constants.MSISDN,msisdn);
        params.put(Constants.POINT,point);
        params.put(Constants.TIME,time);
        params.put(Constants.USERID,id);
        return params;
    }

    private Response.Listener<ScoreObject> createRequestSuccessListener() {
        return new Response.Listener<ScoreObject>() {
            @Override
            public void onResponse(ScoreObject response) {
                Log.d(TAG, "Response value " );
                //Toast.makeText(getApplicationContext(),"onresponse",Toast.LENGTH_SHORT).show();
                try {

                    if (response != null) {
                        Log.d(TAG, "Response value " + response);
                        Toast.makeText(getApplicationContext(),""+response,Toast.LENGTH_LONG).show();

                    } else {
                        displayErrorMessage(ResultActivity.this, "Quiz score failed to upload");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }*/
}