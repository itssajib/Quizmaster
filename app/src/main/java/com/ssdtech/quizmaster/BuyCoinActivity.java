package com.ssdtech.quizmaster;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ssdtech.quizmaster.entities.UserCoins;
import com.ssdtech.quizmaster.entities.UserObject;
import com.ssdtech.quizmaster.interfaces.ActivityInteractionListener;
import com.ssdtech.quizmaster.network.GsonRequest;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.JavaScriptInterface;
import com.ssdtech.quizmaster.utils.Log;
import com.ssdtech.quizmaster.utils.Variables;

public class BuyCoinActivity extends AppCompatActivity implements ActivityInteractionListener{

    private static final String TAG = BuyCoinActivity.class.getSimpleName();

    private WebView mWebview;
    private TextView profileName, profileCoin;
    private ImageView profileImage;
    private View backbtn;
    private UserObject userDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_coin);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        profileImage = (ImageView) findViewById(R.id.profileImage);
        profileName = (TextView) findViewById(R.id.profileName);
        profileCoin = (TextView) findViewById(R.id.profileCoin);

        backbtn = findViewById(R.id.backbtn);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

            }
        });

        mWebview = (WebView) findViewById(R.id.buy_coin_webview);

        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.getSettings().setPluginState(WebSettings.PluginState.ON);
        //mWebview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mWebview.getSettings().setDomStorageEnabled(true);
        mWebview.getSettings().setDatabaseEnabled(true);
        mWebview.setLongClickable(true);
        mWebview.getSettings().setLoadWithOverviewMode(true);
        //mWebview.getSettings().setUseWideViewPort(true);
        mWebview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        //mWebview.getSettings().setBuiltInZoomControls(true);
        //mWebview.getSettings().setDisplayZoomControls(true);
        //mWebview.setVerticalScrollBarEnabled(false);
        mWebview.setHorizontalScrollBarEnabled(false);
        mWebview.addJavascriptInterface(new JavaScriptInterface(BuyCoinActivity.this,this), "Android");

        mWebview.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                //handler.proceed();
                String msg = "";
                final AlertDialog.Builder builder = new AlertDialog.Builder(BuyCoinActivity.this);

                if (error.getPrimaryError() == SslError.SSL_DATE_INVALID
                        || error.getPrimaryError() == SslError.SSL_EXPIRED
                        || error.getPrimaryError() == SslError.SSL_IDMISMATCH
                        || error.getPrimaryError() == SslError.SSL_INVALID
                        || error.getPrimaryError() == SslError.SSL_NOTYETVALID
                        || error.getPrimaryError() == SslError.SSL_UNTRUSTED) {
                    if (error.getPrimaryError() == SslError.SSL_DATE_INVALID) {
                        msg = "The date of the certificate is invalid";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    } else if (error.getPrimaryError() == SslError.SSL_INVALID) {
                        msg = "A generic error occurred";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    } else if (error.getPrimaryError() == SslError.SSL_EXPIRED) {
                        msg = "The certificate has expired";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    } else if (error.getPrimaryError() == SslError.SSL_IDMISMATCH) {
                        msg = "Hostname mismatch";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    } else if (error.getPrimaryError() == SslError.SSL_NOTYETVALID) {
                        msg = "The certificate is not yet valid";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    } else if (error.getPrimaryError() == SslError.SSL_UNTRUSTED) {
                        msg = "The certificate authority is not trusted";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.d(TAG, "onPageStarted " + url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }

        });

        mWebview.setWebChromeClient(new WebChromeClient());

        userDetails = ((CustomApplication) getApplication()).getLoginUser();
        android.util.Log.d(TAG, userDetails.getMsisdn());

        profileName.setText(userDetails.getNames());
        getUserCoins(userDetails.getMsisdn());

        Glide.with(BuyCoinActivity.this).load(Constants.PUBLIC_FOLDER + userDetails.getMood_avatar()).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().into(profileImage);

        String urltoload = Constants.PATH_TO_BUY_COIN + "/" + userDetails.getMsisdn();
        mWebview.loadUrl(urltoload);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserCoins(userDetails.getMsisdn());
    }

    private void getUserCoins(String msisdn) {
        Log.d(TAG, "getUserCoins");
        String URL = Constants.PATH_TO_GET_USER_COIN + "/" + msisdn;
        GsonRequest<UserCoins> serverRequest = new GsonRequest<>(
                Request.Method.POST,
                URL,
                UserCoins.class,
                null,
                getUserCoinsSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Response.Listener<UserCoins> getUserCoinsSuccessListener() {
        Log.d(TAG, "getUserCoins Response");
        return new Response.Listener<UserCoins>() {
            @Override
            public void onResponse(UserCoins response) {
                try {
                    if (response != null) {
                        Log.d(TAG, "getUserCoins : " + response.getUserCoin());
                        profileCoin.setText(response.getUserCoin());
                        Variables.COIN=Integer.parseInt(response.getUserCoin());
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    @Override
    public void onRefresh() {

        startActivity(new Intent(this,BuyCoinActivity.class));
        finish();
       Log.d(TAG,"refresh");
        //getUserCoins(userDetails.getMsisdn());
    }
}

