package com.ssdtech.quizmaster;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.github.pavlospt.CircleView;
import com.google.gson.Gson;
import com.ssdtech.quizmaster.entities.ResultWrapper;
import com.ssdtech.quizmaster.entities.ScoreObject;
import com.ssdtech.quizmaster.entities.UserCoins;
import com.ssdtech.quizmaster.entities.UserObject;
import com.ssdtech.quizmaster.network.GsonRequest;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.DisplayMessage;
import com.ssdtech.quizmaster.utils.Helper;
import com.ssdtech.quizmaster.utils.Log;
import com.ssdtech.quizmaster.utils.Variables;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.ssdtech.quizmaster.utils.DisplayMessage.displayErrorMessage;

public class ResultActivity extends AppCompatActivity {

    private static final String TAG = ResultActivity.class.getSimpleName();

    private String mResult;

    private String quizSubcategoryName;
    private UserObject userObject;

    TextView tvWelcome,tvResult,remaining_coin;
    View buy_coin;
    String result="";
    private String indicator="";
    View result_layout,lets_play_layout,lets_play,gotohome,gotoprofile;
    SharedPreferences prefs;

    public static String indicator1,time,score;
    public  static int totalPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        tvWelcome= (TextView) findViewById(R.id.tvWelcome);
        tvResult= (TextView) findViewById(R.id.tvResult);
        remaining_coin= (TextView) findViewById(R.id.remaining_coin);
        buy_coin=findViewById(R.id.buy_coin);
        result_layout=findViewById(R.id.result_layout);
        lets_play_layout=findViewById(R.id.lets_play_layout);
        lets_play=findViewById(R.id.lets_play);
        gotohome=findViewById(R.id.go_home);
        gotoprofile=findViewById(R.id.go_profile);
        Log.d(TAG,"send_net1");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        prefs = this.getSharedPreferences(Constants.MYPREF, Context.MODE_PRIVATE);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            quizSubcategoryName = bundle.getString(Constants.SUBCATEGORY);
        }

        userObject = ((CustomApplication) getApplication()).getLoginUser();
        mResult = getIntent().getExtras().getString(Constants.QUIZ_RESULT);
        indicator=getIntent().getStringExtra(Constants.INDICATOR);
        if(indicator.equals(Constants.FROM_SIGNUP)){
            result_layout.setVisibility(View.GONE);
            lets_play_layout.setVisibility(View.VISIBLE);
        }else{
            result_layout.setVisibility(View.VISIBLE);
            lets_play_layout.setVisibility(View.GONE);
        }

        //registerReceiver(broadcastReceiver, new IntentFilter("INTERNET_FOUND"));
        Gson gson = ((CustomApplication) getApplication()).getGsonObject();
        ResultWrapper[] mWrapper = gson.fromJson(mResult, ResultWrapper[].class);
        List<ResultWrapper> submittedResult = arrayToListObject(mWrapper);

        int scoreInPercentage = getScorePercent(submittedResult);
        totalPoint= getTotalPoint(submittedResult);

        CircleView circleView = (CircleView) findViewById(R.id.your_score);
        circleView.setTitleText(scoreInPercentage + "%");

        TextView correctAnswers = (TextView) findViewById(R.id.correct_answer);
        int passCount = questionCount(submittedResult, true);
        if (passCount == 0) {
            correctAnswers.setText(String.format(Locale.getDefault(), "%d correct answer", passCount));
        } else {
            correctAnswers.setText(String.format(Locale.getDefault(), "%d correct answers", passCount));
        }

        TextView wrongAnswers = (TextView) findViewById(R.id.wrong_answer);
        int failCount = questionCount(submittedResult, false);
        if (failCount == 0) {
            wrongAnswers.setText(String.format(Locale.getDefault(), "%d wrong answer", failCount));
        } else {
            wrongAnswers.setText(String.format(Locale.getDefault(), "%d wrong answers", failCount));
        }

        String times=getTimeTaken(submittedResult);
        result= "Right answer   :  "+passCount+"  \n\n"+
                "Wrong answer   :  "+failCount+"  \n\n"+
                "Time taken     :  "+times+"s\n\n"+
                "Point          :  "+totalPoint;

        tvResult.setText(result);
        tvWelcome.setText("Well played, "+userObject.getNames());
        getUserCoins(userObject.getMsisdn());

        Log.d(TAG,"send_net1");

        time=times;
        indicator1="result";
        if(isOnline()){
            getMSISDN();
        }else{
            Toast.makeText(getApplicationContext(),getString(R.string.need_robi_network),Toast.LENGTH_LONG).show();
        }

        /*if (Helper.isNetworkAvailable(ResultActivity.this)) {
            Log.d(TAG,"send_net");
          // Toast.makeText(getApplicationContext(),"send_in_net",Toast.LENGTH_SHORT).show();
            if(!indicator.equals(Constants.FROM_SIGNUP)){
                sendScoreToServer(quizSubcategoryName, userObject.getNames(), userObject.getEmail(), String.valueOf(scoreInPercentage),userObject.getMsisdn(),""+passCount*5,time,""+userObject.getId());
                //prefs.edit().putString(Constants.SCOREOBJECT,"0");
            }
        } */

        /*Button resultButton = (Button) findViewById(R.id.result_button);
        resultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent analysisIntent = new Intent(ResultActivity.this, AnalysisActivity.class);
                analysisIntent.putExtra(Constants.QUIZ_RESULT, mResult);
                startActivity(analysisIntent);
            }
        });
*/

        buy_coin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                indicator1="buy_coin";
                if(isOnline()){
                    getMSISDN();
                }else{
                    Toast.makeText(getApplicationContext(),getString(R.string.need_robi_network),Toast.LENGTH_LONG).show();
                }
            }
        });

        View mainMenuButton =  findViewById(R.id.main_menu_button);
        mainMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainMenuIntent = new Intent(ResultActivity.this, MainActivityHome.class);
                startActivity(mainMenuIntent);
                finish();
            }
        });

        View playAgainButton =  findViewById(R.id.play_again_button);
        playAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get current quiz subcategory details
                String currentQuiz = ((CustomApplication) getApplication()).getShared().getCurrentQuiz();

                Intent selectedIntent = new Intent(ResultActivity.this, QuizDetailActivity.class);
                selectedIntent.putExtra(Constants.SUBCATEGORY, currentQuiz);
                startActivity(selectedIntent);
                finish();
            }
        });

        lets_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentQuiz = ((CustomApplication) getApplication()).getShared().getCurrentQuiz();
                Intent selectedIntent = new Intent(ResultActivity.this, QuizDetailActivity.class);
                selectedIntent.putExtra(Constants.SUBCATEGORY, currentQuiz);
                selectedIntent.putExtra(Constants.INDICATOR,"");
                startActivity(selectedIntent);
                finish();
            }
        });

        gotoprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selectedIntent = new Intent(ResultActivity.this, ProfileActivity.class);
                startActivity(selectedIntent);
                finish();
            }
        });

        gotohome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selectedIntent = new Intent(ResultActivity.this, MainActivityHome.class);
                startActivity(selectedIntent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {

    }

    private List<ResultWrapper> arrayToListObject(ResultWrapper[] response) {
        List<ResultWrapper> allCategories = new ArrayList<>();
        Collections.addAll(allCategories, response);
        return allCategories;
    }

    private int questionCount(List<ResultWrapper> result, boolean passed) {
        int correct = 0;
        int wrong = 0;

        for (int i = 0; i < result.size(); i++) {
            if (result.get(i).getIsAnswer() == 1) {
                correct++;
            } else {
                wrong++;
            }
        }
        if (passed) {
            return correct;
        }
        return wrong;
    }

    private String getTimeTaken(List<ResultWrapper> result) {
        String time="" ;

        for (int i = 0; i < result.size(); i++) {
            time=result.get(i).getTime();
        }
        return time;
    }

    private int getScorePercent(List<ResultWrapper> result) {
        int count = 0;
        if (result == null) {
            return 0;
        }
        for (int i = 0; i < result.size(); i++) {
            if (result.get(i).getIsAnswer() == 1) {
                count++;
            }
        }
        return (int) ((count * 100) / result.size());
    }

    private int getTotalPoint(List<ResultWrapper> result) {
        int count = 0;
        if (result == null) {
            return 0;
        }
        for (int i = 0; i < result.size(); i++) {
            if (result.get(i).getPoint() != null) {
                count+=Integer.parseInt(result.get(i).getPoint());
            }
        }
        return count;
    }


    private void sendScoreToServer(String subcategory, String username, String email, String score,String msisdn,String point,String time,String id) {

        Map<String, String> params = getParams(subcategory, username, email, score,msisdn,point,time,id);
        Log.d(TAG, "sendvalue "+params.toString() );
        GsonRequest<ScoreObject> serverRequest = new GsonRequest<ScoreObject>(
                Request.Method.POST,
                Constants.PATH_TO_ADD_SCORE,
                ScoreObject.class,
                params,
                createRequestSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Map<String, String> getParams(String subcategory, String username, String email, String score,String msisdn,String point,String time,String id) {
        Map<String, String> params = new HashMap<String, String>();
        //params.put(Constants.SUBCATEGORY, subcategory);
        params.put(Constants.NAME, username);
        params.put(Constants.EMAIL, email);
        params.put(Constants.SCORE, score);
        params.put(Constants.MSISDN,msisdn);
        params.put(Constants.POINT,point);
        params.put(Constants.TIME,time);
        params.put(Constants.USERID,id);
        return params;
    }

    private void getUserCoins(String msisdn) {
        Log.d(TAG, "getUserCoins");
        String URL = Constants.PATH_TO_GET_USER_COIN + "/" + msisdn;
        GsonRequest<UserCoins> serverRequest = new GsonRequest<>(
                Request.Method.POST,
                URL,
                UserCoins.class,
                null,
                getUserCoinsSuccessListener(),
                createRequestErrorListener());
        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private Response.Listener<UserCoins> getUserCoinsSuccessListener() {
        Log.d(TAG, "getUserCoins Response");
        return new Response.Listener<UserCoins>() {
            @Override
            public void onResponse(UserCoins response) {
                try {
                    if (response != null) {
                        Log.d(TAG, "getUserCoins : " + response.getUserCoin());
                        remaining_coin.setText("Coins available: " + response.getUserCoin());
                        Variables.COIN=Integer.parseInt(response.getUserCoin());
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.Listener<ScoreObject> createRequestSuccessListener() {
        return new Response.Listener<ScoreObject>() {
            @Override
            public void onResponse(ScoreObject response) {
                Log.d(TAG, "Response value " );
                //Toast.makeText(getApplicationContext(),"onresponse",Toast.LENGTH_SHORT).show();
                try {

                    if (response != null) {
                        Log.d(TAG, "Response value " + response);
                        Toast.makeText(getApplicationContext(),""+response,Toast.LENGTH_LONG).show();

                    } else {
                        displayErrorMessage(ResultActivity.this, "Quiz score failed to upload");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    private void getMSISDN() {
        Log.d(TAG, "getting MSISDN......");
        GsonRequest<UserObject> serverRequest = new GsonRequest<UserObject>(
                Request.Method.POST,
                Constants.PATH_TO_GRAB_MSISDN,
                UserObject.class,
                createRequestSuccessListenermsisdn(),
                createRequestErrorListener());

        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private Response.Listener<UserObject> createRequestSuccessListenermsisdn() {
        return new Response.Listener<UserObject>() {
            @Override
            public void onResponse(UserObject response) {
                try {
                    Log.d(TAG, "getting MSISDN : " + response);
                    if (response != null) {

                        if (response.getMsisdn() != null && !response.getMsisdn().equals("") && !response.getMsisdn().equals("null") && response.getMsisdn().length() > 9) {
                            if(indicator1.equals("buy_coin")){
                                Intent loginIntent = new Intent(getApplicationContext(), BuyCoinActivity.class);
                                loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(loginIntent);
                                finish();
                            }else if(indicator1.equals("result")){
                                if(!indicator.equals(Constants.FROM_SIGNUP)){
                                    sendScoreToServer(quizSubcategoryName, userObject.getNames(), userObject.getEmail(), "0",userObject.getMsisdn(),""+totalPoint,time,""+userObject.getId());
                                }
                            }


                        } else {
                            displayErrorMessage(ResultActivity.this, getString(R.string.need_robi_network));
                        }

                    } else {
                        Log.d(TAG, "getting MSISDN : null");
                        displayErrorMessage(ResultActivity.this, getString(R.string.need_robi_network));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /*BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // internet lost alert dialog method call from here...
            //Log.d();

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
             String value = preferences.getString(Constants.SCOREOBJECT, "0");
             if(!value.equals(0)){
                 try {
                     JSONObject object = new JSONObject(value);
                     Log.d(TAG,"score_object"+object.toString());
                     sendScoreToServer(object.getString("quizSubcategoryName"), object.getString("username"), object.getString("email"), object.getString("scoreInPercentage"),object.getString("msisdn"),object.getString("point"),object.getString("time"),object.getString("userid"));
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }
             }
        }
    };*/
}

