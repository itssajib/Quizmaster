package com.ssdtech.quizmaster.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ssdtech.quizmaster.R;
import com.ssdtech.quizmaster.entities.TopScoreObject;

import java.util.List;


public class LadderBoardAdapter extends RecyclerView.Adapter<LadderBoardAdapter.DataObjectHolder> {

    private static String TAG = LadderBoardAdapter.class.getSimpleName();
    private List<TopScoreObject> mDataset;
    //private static MyClickListener myClickListener;


    public static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView score;
        ImageView image;

        public DataObjectHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.profileName);
            score = (TextView) itemView.findViewById(R.id.profileScore);
            image = (ImageView) itemView.findViewById(R.id.profileImage);
        }
    }

    public LadderBoardAdapter(List<TopScoreObject> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ladder_board_list_item, parent, false);
        return new DataObjectHolder(view);
    }


    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        Log.d(TAG, "Display " + mDataset.get(position).getUsername() + " " + mDataset.get(position).getScores());
        holder.name.setText(mDataset.get(position).getUsername());
        holder.score.setText(String.format("%s points", mDataset.get(position).getScores()));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
