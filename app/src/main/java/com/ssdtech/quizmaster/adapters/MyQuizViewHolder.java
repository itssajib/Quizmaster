package com.ssdtech.quizmaster.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ssdtech.quizmaster.R;

public class MyQuizViewHolder extends RecyclerView.ViewHolder{

    public ImageView quizImage;
    public TextView quizName;
    public View view;

    public MyQuizViewHolder(View itemView) {
        super(itemView);
        quizImage = (ImageView)itemView.findViewById(R.id.selected_quiz_image);
        quizName = (TextView)itemView.findViewById(R.id.selected_quiz_name);
        view = itemView;
    }
}
