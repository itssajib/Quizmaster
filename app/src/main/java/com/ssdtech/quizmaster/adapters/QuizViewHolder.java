package com.ssdtech.quizmaster.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ssdtech.quizmaster.R;

public class QuizViewHolder extends RecyclerView.ViewHolder{

    public ImageView quizIcon;
    public TextView quizTitle;
    public TextView quizDescription;
    public CheckBox quizSelected;

    public LinearLayout linearLayout;

    public QuizViewHolder(View itemView) {
        super(itemView);

        quizIcon = (ImageView)itemView.findViewById(R.id.quiz_category_icon);
        quizTitle = (TextView) itemView.findViewById(R.id.quiz_category_name);
        quizDescription = (TextView) itemView.findViewById(R.id.quiz_category_description);
        quizSelected = (CheckBox)itemView.findViewById(R.id.quiz_selected);
        linearLayout = (LinearLayout)itemView.findViewById(R.id.click_layer);
    }
}
