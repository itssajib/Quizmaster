package com.ssdtech.quizmaster.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ssdtech.quizmaster.R;
import com.ssdtech.quizmaster.entities.AvatarObject;
import com.ssdtech.quizmaster.interfaces.AvatarActivityListener;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.Log;

import java.util.List;


public class AvatarRecyclerViewAdapter extends RecyclerView.Adapter<AvatarRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = AvatarRecyclerViewAdapter.class.getSimpleName();
    private List<AvatarObject> mValues;
    private final AvatarActivityListener mListener;
    private AvatarRecyclerViewAdapter mAdapter;
    private Context context;

    public static int selectedView;

    public AvatarRecyclerViewAdapter(List<AvatarObject> items, AvatarActivityListener listener) {
        mValues = items;
        mListener = listener;
        mAdapter = this;
    }

    @Override
    public AvatarRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gift_recyclerview_single_item, parent, false);
        context = view.getContext();
        return new AvatarRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AvatarRecyclerViewAdapter.ViewHolder holder, final int position) {

        try {
           /* int width = (Variables.DISPLAY_WIDTH / 4);
            int height = (int) (width * 1.25);
            holder.mImg.getLayoutParams().height = width;
            holder.mImg.getLayoutParams().width = width;*/
            Log.d(TAG, "Position " + position);
            holder.mItem = mValues.get(position);

            final String serverImagePath = holder.mItem.getImagePath();
            Log.d(TAG, "ImgPath : " + serverImagePath);

            Glide.with(context).load(Constants.PUBLIC_FOLDER + serverImagePath).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().into(holder.mImg);

            holder.mImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onSelectedImagePath(serverImagePath);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImg;
        public AvatarObject mItem;
        public String contentUrl;
        LinearLayout mode_layout;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImg = (ImageView) view.findViewById(R.id.gift_img);
            mode_layout = (LinearLayout) view.findViewById(R.id.mode_layout);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}
