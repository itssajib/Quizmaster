package com.ssdtech.quizmaster.adapters;


import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ssdtech.quizmaster.R;
import com.ssdtech.quizmaster.ResultActivity;
import com.ssdtech.quizmaster.entities.ResultWrapper;

import java.util.List;

public class ResultAdapter extends RecyclerView.Adapter<ResultViewHolder>{

    private static final String TAG = ResultActivity.class.getSimpleName();

    private List<ResultWrapper> resultObjectList;

    public ResultAdapter(List<ResultWrapper> resultObjectList) {
        this.resultObjectList = resultObjectList;
    }

    @Override
    public ResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quiz_result_layout, parent, false);
        return new ResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ResultViewHolder holder, int position) {
        ResultWrapper rObject = resultObjectList.get(position);

        holder.question.setText(rObject.getQuestionNumber() + ". " + rObject.getQuestion());
        holder.correct.setText(String.format("Correct Answer: %s", rObject.getCorrect()));
        if(TextUtils.isEmpty(rObject.getWrong())){
            holder.wrong.setText("Selected Answer: No answer selected");
        }else{
            holder.wrong.setText(String.format("Selected Answer: %s", rObject.getWrong()));
        }

        if(rObject.getIsAnswer() == 1){
            holder.mark.setImageResource(R.drawable.correct);
        }else{
            holder.mark.setImageResource(R.drawable.wrong);
        }
    }

    @Override
    public int getItemCount() {
        return resultObjectList.size();
    }
}
