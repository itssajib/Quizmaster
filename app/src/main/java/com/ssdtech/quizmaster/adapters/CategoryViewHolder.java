package com.ssdtech.quizmaster.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ssdtech.quizmaster.R;

public class CategoryViewHolder extends RecyclerView.ViewHolder{

    public ImageView quizImage;
    public TextView quizName;
    public TextView quizCategoryName;
    public View category_item;
    

    public CategoryViewHolder(View itemView) {
        super(itemView);

        quizImage = (ImageView)itemView.findViewById(R.id.quiz_image);
        quizName = (TextView)itemView.findViewById(R.id.quiz_name);
        quizCategoryName = (TextView)itemView.findViewById(R.id.quiz_category);
        category_item = itemView.findViewById(R.id.category_item);
    }
}
