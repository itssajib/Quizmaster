package com.ssdtech.quizmaster.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ssdtech.quizmaster.R;

public class PerformanceViewHolder extends RecyclerView.ViewHolder{

    public TextView quizCategory;
    public TextView  quizCategoryScore;
    public ProgressBar scoreIndicator;


    public PerformanceViewHolder(View itemView) {
        super(itemView);
        quizCategory = (TextView)itemView.findViewById(R.id.quiz_subject);
        quizCategoryScore = (TextView)itemView.findViewById(R.id.quiz_subject_score);
        scoreIndicator = (ProgressBar)itemView.findViewById(R.id.quiz_score_indicator);
    }
}
