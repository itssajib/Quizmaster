package com.ssdtech.quizmaster.adapters;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.ssdtech.quizmaster.R;
import com.ssdtech.quizmaster.SelectQuizActivity;
import com.ssdtech.quizmaster.entities.CategoryObject;
import com.ssdtech.quizmaster.entities.CheckObject;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.Helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class QuizAdapter extends RecyclerView.Adapter<QuizViewHolder>{

    private static final String TAG = QuizAdapter.class.getSimpleName();

    private Context context;
    private List<CategoryObject> categoryObjectList;

    private List<CheckObject> markList = null;

    public QuizAdapter(Context context, List<CategoryObject> categoryObjectList) {
        this.context = context;
        this.categoryObjectList = categoryObjectList;
    }

    @Override
    public QuizViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.select_quiz_layout, parent, false);
        return new QuizViewHolder(view);
    }

    @Override
    public void onBindViewHolder(QuizViewHolder holder, int position) {
        CategoryObject cObject = categoryObjectList.get(position);

        final int catDbId = cObject.getId();
        final String catName = cObject.getName();
        final String catImage = cObject.getImage();

        String serverImagePath = Constants.PUBLIC_FOLDER + cObject.getImage();
        Glide.with(context).load(serverImagePath).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().override(48, 48).into(holder.quizIcon);

        holder.quizTitle.setText(cObject.getName());
        holder.quizDescription.setText(Helper.cutLongText(cObject.getDescription()));

        // set all the selected check box
        holder.quizSelected.setTag(String.valueOf(catDbId));

        final Gson gson = ((CustomApplication)((Activity)context).getApplication()).getGsonObject();
        String checkList = ((CustomApplication)((Activity)context).getApplication()).getShared().getCheckQuiz();
        if(!TextUtils.isEmpty(checkList)){
            CheckObject[] checkObjects = gson.fromJson(checkList, CheckObject[].class);
            markList = new LinkedList<>(Arrays.asList(checkObjects));

            for(int i = 0; i < markList.size(); i++){
                if(markList.get(i).getId() == catDbId){
                    holder.quizSelected.setChecked(true);
                }
            }
        }else{
            markList = new ArrayList<>();
        }

        holder.quizSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    markList.add(new CheckObject(catDbId, true, catName, catImage));
                }else{
                    CheckBox mBox = (CheckBox)buttonView;
                    int tagNumber = Integer.parseInt((String)mBox.getTag());
                    for(int j = 0; j < markList.size(); j++){
                        if(markList.get(j).getId() == tagNumber){
                            markList.remove(j);
                        }
                    }
                }
                String saveMarkList = gson.toJson(markList);
                ((CustomApplication)((Activity)context).getApplication()).getShared().setCheckQuiz(saveMarkList);
            }
        });

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selectedQuizIntent = new Intent(context, SelectQuizActivity.class);

                selectedQuizIntent.putExtra(Constants.CATEGORY, catDbId);
                selectedQuizIntent.putExtra(Constants.CATEGORY_NAME, catName);
                selectedQuizIntent.putExtra(Constants.CATEGORY_IMAGE, catImage);

                context.startActivity(selectedQuizIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryObjectList.size();
    }
}
