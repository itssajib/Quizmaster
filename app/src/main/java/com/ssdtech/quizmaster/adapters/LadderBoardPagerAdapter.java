package com.ssdtech.quizmaster.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ssdtech.quizmaster.fragment.DailyTopScorerFragment;
import com.ssdtech.quizmaster.fragment.TopScorerFragment;


public class LadderBoardPagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public LadderBoardPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                DailyTopScorerFragment tab1 = new DailyTopScorerFragment();
                return tab1;
            case 1:
                TopScorerFragment tab2 = new TopScorerFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
