package com.ssdtech.quizmaster.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ssdtech.quizmaster.SelectQuizActivity;
import com.ssdtech.quizmaster.entities.CheckObject;
import com.ssdtech.quizmaster.utils.Constants;

import java.util.List;

public class MyQuizAdapter extends RecyclerView.Adapter<MyQuizViewHolder>{

    private Context context;
    private List<CheckObject> quizList;
    private int layoutResource;

    public MyQuizAdapter(Context context, List<CheckObject> quizList, int layout) {
        this.context = context;
        this.quizList = quizList;
        this.layoutResource = layout;
    }

    @Override
    public MyQuizViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutResource, parent, false);
        return new MyQuizViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyQuizViewHolder holder, int position) {
        final CheckObject checkObject = quizList.get(position);
        holder.quizName.setText(checkObject.getName());

        String serverImagePath = Constants.PUBLIC_FOLDER + checkObject.getImage();
        Glide.with(context).load(serverImagePath).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().override(60, 60).into(holder.quizImage);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileIntent = new Intent(context, SelectQuizActivity.class);
                profileIntent.putExtra(Constants.CATEGORY, checkObject.getId());
                profileIntent.putExtra(Constants.CATEGORY_NAME, checkObject.getName());
                profileIntent.putExtra(Constants.CATEGORY_IMAGE, checkObject.getImage());
                context.startActivity(profileIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return quizList.size();
    }
}
