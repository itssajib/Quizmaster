package com.ssdtech.quizmaster.adapters;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.ssdtech.quizmaster.QuizDetailActivity;
import com.ssdtech.quizmaster.R;
import com.ssdtech.quizmaster.entities.SingleQuizObject;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.Log;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryViewHolder>{

    private static final String TAG = CategoryAdapter.class.getSimpleName();

    private Context context;
    String category;
    private List<SingleQuizObject> singleQuizObjectList;

    public CategoryAdapter(Context context, List<SingleQuizObject> singleQuizObjectList) {
        this.context = context;
        this.singleQuizObjectList = singleQuizObjectList;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_layout, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        final SingleQuizObject sObject = singleQuizObjectList.get(position);
        category = sObject.getCategory();
        Gson gson = ((CustomApplication)((Activity)context).getApplication()).getGsonObject();
        final String subcategoryObject = gson.toJson(sObject);

        String serverImagePath = Constants.PUBLIC_FOLDER + sObject.getImage();
        Glide.with(context).load(serverImagePath).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().override(80, 80).into(holder.quizImage);

        holder.quizName.setText(sObject.getName());
        //holder.quizCategoryName.setText(sObject.getCategory());

        holder.category_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detailIntent = new Intent(context, QuizDetailActivity.class);
               // detailIntent.putExtra(Constants.SUBCATEGORY, subcategoryObject);
                detailIntent.putExtra(Constants.CATEGORY, sObject.getName());
                Log.d(TAG,"category"+sObject.getName());
                context.startActivity(detailIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return singleQuizObjectList.size();
    }
}
