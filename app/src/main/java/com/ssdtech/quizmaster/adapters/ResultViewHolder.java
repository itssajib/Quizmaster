package com.ssdtech.quizmaster.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ssdtech.quizmaster.R;

public class ResultViewHolder extends RecyclerView.ViewHolder{

    public TextView question;
    public TextView correct;
    public TextView wrong;
    public ImageView mark;

    public ResultViewHolder(View itemView) {
        super(itemView);

        question = (TextView)itemView.findViewById(R.id.question);
        correct = (TextView)itemView.findViewById(R.id.correct_answer);
        wrong = (TextView)itemView.findViewById(R.id.wrong_answer);
        mark = (ImageView)itemView.findViewById(R.id.mark_icon);
    }
}
