package com.ssdtech.quizmaster.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ssdtech.quizmaster.R;
import com.ssdtech.quizmaster.entities.PerformanceObject;

import java.util.List;

public class PerformanceAdapter extends RecyclerView.Adapter<PerformanceViewHolder>{

    private Context context;
    private List<PerformanceObject> performanceObjectList;

    public PerformanceAdapter(Context context, List<PerformanceObject> performanceObjectList) {
        this.context = context;
        this.performanceObjectList = performanceObjectList;
    }

    @Override
    public PerformanceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.performance_layout, parent, false);
        return new PerformanceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PerformanceViewHolder holder, int position) {
        PerformanceObject performanceObject = performanceObjectList.get(position);

        holder.quizCategory.setText(performanceObject.getName());
        holder.quizCategoryScore.setText(String.format("%s%% in all quiz attempts", performanceObject.getValue()));
        holder.scoreIndicator.setProgress(Integer.parseInt(performanceObject.getValue()));

    }

    @Override
    public int getItemCount() {
        return performanceObjectList.size();
    }
}
