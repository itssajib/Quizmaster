package com.ssdtech.quizmaster;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ssdtech.quizmaster.adapters.AvatarRecyclerViewAdapter;
import com.ssdtech.quizmaster.entities.AvatarObject;
import com.ssdtech.quizmaster.entities.ResponseObject;
import com.ssdtech.quizmaster.entities.UserObject;
import com.ssdtech.quizmaster.interfaces.AvatarActivityListener;
import com.ssdtech.quizmaster.network.GsonRequest;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.Log;
import com.ssdtech.quizmaster.utils.Variables;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ssdtech.quizmaster.utils.DisplayMessage.displayErrorMessage;

public class AvatarActivity extends AppCompatActivity implements AvatarActivityListener {

    private static final String TAG = AvatarActivity.class.getSimpleName();

    private View update_button;
    private TextView name;
    private RecyclerView recyclerView;
    private ImageView selectImage;

    private static String imagePath = "";
    private UserObject userDetails;

    private AvatarActivityListener mListener;
    String indicator="";
    View avatar_layout,congratulation_layout,click_here,save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avatar);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        mListener = this;

        userDetails = ((CustomApplication) getApplication()).getLoginUser();
        Log.d(TAG, userDetails.getMsisdn());

        update_button = findViewById(R.id.avatar_update_button);
        name = (TextView) findViewById(R.id.name);
        selectImage = (ImageView) findViewById(R.id.selectImage);
        recyclerView = (RecyclerView) findViewById(R.id.avatar_recyclerview);
        avatar_layout=findViewById(R.id.avatar_layout);
        congratulation_layout=findViewById(R.id.congratulation_layout);
        click_here=findViewById(R.id.click_here);
        save=findViewById(R.id.save);

        indicator=getIntent().getStringExtra(Constants.INDICATOR);
        Log.d(TAG,indicator);
        name.setText(userDetails.getNames());
        GridLayoutManager layoutManager = new GridLayoutManager(AvatarActivity.this, 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setFocusable(false);


        if(indicator!=null && !indicator.equals(Constants.FROM_SIGNUP)){
            update_button.setVisibility(View.GONE);
            save.setVisibility(View.VISIBLE);
        }

        update_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!imagePath.equals("")) {
                    Log.d(TAG, "Upding user image");
                    updateUserImage(userDetails.getMsisdn(), imagePath);
                }
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!imagePath.equals("")) {
                    Log.d(TAG, "Upding user image");
                    updateUserImage(userDetails.getMsisdn(), imagePath);
                }
            }
        });

        click_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), QuizActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.INDICATOR,Constants.FROM_SIGNUP);
                intent.putExtra(Constants.QUIZ_NAME,"WARMUP");
                startActivity(intent);
                finish();
            }
        });

        getMoodavatarList(userDetails.getMsisdn());
    }

    private void updateUserImage(String msisdn, String imagePath) {
        Log.d(TAG, "updateUserImage");
        Map params = getUpdateParams(msisdn, imagePath);
        GsonRequest<ResponseObject> serverRequest = new GsonRequest<>(
                Request.Method.POST,
                Constants.PATH_TO_SAVE_MOOD_AVATAR,
                ResponseObject.class,
                params,
                createUpdateSuccessListener(),
                createUpdatetErrorListener());

        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Response.ErrorListener createUpdatetErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    private Response.Listener<ResponseObject> createUpdateSuccessListener() {
        Log.d(TAG, "updateUserImage Response");
        return new Response.Listener<ResponseObject>() {
            @Override
            public void onResponse(ResponseObject response) {
                try {
                    if (response != null) {
                        Log.d(TAG, "updateUserImage : " + response.getResponse());
                        String userinfo = ((CustomApplication) getApplication()).getLoginUserString();
                        Log.d(TAG, "userInfo : " + userinfo);
                        JSONObject userobj = new JSONObject(userinfo);
                        userobj.put("mood_avatar", imagePath);
                        Log.d(TAG, "userInfo updated: " + userobj.toString());
                        ((CustomApplication) getApplication()).getShared().setUserData(userobj.toString());
                        if(indicator.equals(Constants.FROM_SIGNUP)){
                            avatar_layout.setVisibility(View.GONE);
                            congratulation_layout.setVisibility(View.VISIBLE);
                        }else{
                            ((CustomApplication) getApplication()).redirectLoginUsers();
                        }

                    } else {
                        displayErrorMessage(AvatarActivity.this, "Error! Registration failed. Try again");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Map getUpdateParams(String msisdn, String path) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.MSISDN, msisdn);
        params.put(Constants.IMAGEPATH, path);
        return params;
    }


    private void getMoodavatarList(String msisdn) {
        Log.d(TAG, "getMoodavatarList");
        Map params = getParams(msisdn);
        GsonRequest<AvatarObject[]> serverRequest = new GsonRequest<>(
                Request.Method.POST,
                Constants.PATH_TO_GET_MOOD_AVATAR,
                AvatarObject[].class,
                params,
                createRequestSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Map getParams(String msisdn) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.MSISDN, msisdn);
        return params;
    }

    private Response.Listener<AvatarObject[]> createRequestSuccessListener() {
        Log.d(TAG, "getMoodavatarList Response");
        return new Response.Listener<AvatarObject[]>() {
            @Override
            public void onResponse(AvatarObject[] response) {
                try {
                    if (response != null) {
                        List<AvatarObject> avatarObjects = arrayToListObject(response);
                        Log.d(TAG, "getMoodavatarList : " + avatarObjects.size());
                        recyclerView.setAdapter(new AvatarRecyclerViewAdapter(avatarObjects, mListener));
                    } else {
                        displayErrorMessage(AvatarActivity.this, "Error! Registration failed. Try again");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    private List<AvatarObject> arrayToListObject(AvatarObject[] response) {
        List<AvatarObject> allCategories = new ArrayList<>();
        Collections.addAll(allCategories, response);
        return allCategories;
    }

    @Override
    public void onSelectedImagePath(String path) {
        Log.d(TAG, "onSelectedImagePath :" + path);
        imagePath = path;
        Glide.with(AvatarActivity.this).load(Constants.PUBLIC_FOLDER + path).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().into(selectImage);
    }
}
