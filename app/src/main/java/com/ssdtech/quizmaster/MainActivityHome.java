package com.ssdtech.quizmaster;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ssdtech.quizmaster.entities.UserCoins;
import com.ssdtech.quizmaster.entities.UserObject;
import com.ssdtech.quizmaster.interfaces.ActivityInteractionListener;
import com.ssdtech.quizmaster.network.GsonRequest;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.JavaScriptInterface;
import com.ssdtech.quizmaster.utils.Log;
import com.ssdtech.quizmaster.utils.Variables;

import static com.ssdtech.quizmaster.utils.DisplayMessage.displayErrorMessage;

/**
 * Created by Sajib on 12/21/2017.
 */


public class MainActivityHome extends AppCompatActivity implements ActivityInteractionListener{

    private static final String TAG = MainActivityHome.class.getSimpleName();

    private View profiletab, historytab, leaderboardtab, play, buycoin,terms_condition;
    private WebView mWebview;
    private TextView home_user_coin;
    private ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_home);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        profiletab = findViewById(R.id.root_tab_profile);
        historytab = findViewById(R.id.root_tab_history);
        leaderboardtab = findViewById(R.id.root_tab_leader);
        buycoin = findViewById(R.id.buy_coin);
        play = findViewById(R.id.play_quiz);
        mWebview = (WebView) findViewById(R.id.home_webview);
        home_user_coin = (TextView) findViewById(R.id.home_user_coin);
        terms_condition=findViewById(R.id.terms_condition);
        //progressBar = ProgressDialog.show(MainActivityHome.this, "Showing ProgressDialog", "Loading...");

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(getApplicationContext(), SelectQuizActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loginIntent);
            }
        });

        profiletab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(getApplicationContext(), ProfileActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loginIntent);
            }
        });

        leaderboardtab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(getApplicationContext(), LeaderboardActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(loginIntent);
            }
        });

        historytab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(getApplicationContext(), HistoryActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loginIntent);
            }
        });

        buycoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isOnline()){
                    Log.d(TAG,"net_available");
                    getMSISDN();
                }else{
                    Toast.makeText(getApplicationContext(),getString(R.string.need_robi_network),Toast.LENGTH_LONG).show();
                }
            }
        });

        terms_condition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FAQactivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.getSettings().setPluginState(WebSettings.PluginState.ON);
        //mWebview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mWebview.getSettings().setDomStorageEnabled(true);
        mWebview.getSettings().setDatabaseEnabled(true);
        mWebview.setLongClickable(true);
        mWebview.getSettings().setLoadWithOverviewMode(true);
        //mWebview.getSettings().setUseWideViewPort(true);
        mWebview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        // mWebview.getSettings().setBuiltInZoomControls(true);
        //mWebview.getSettings().setDisplayZoomControls(true);
        mWebview.setVerticalScrollBarEnabled(false);
        mWebview.setHorizontalScrollBarEnabled(false);
        mWebview.addJavascriptInterface(new JavaScriptInterface(MainActivityHome.this,this), "Android");

        mWebview.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                //handler.proceed();
                String msg = "";
                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivityHome.this);

                if (error.getPrimaryError() == SslError.SSL_DATE_INVALID
                        || error.getPrimaryError() == SslError.SSL_EXPIRED
                        || error.getPrimaryError() == SslError.SSL_IDMISMATCH
                        || error.getPrimaryError() == SslError.SSL_INVALID
                        || error.getPrimaryError() == SslError.SSL_NOTYETVALID
                        || error.getPrimaryError() == SslError.SSL_UNTRUSTED) {
                    if (error.getPrimaryError() == SslError.SSL_DATE_INVALID) {
                        msg = "The date of the certificate is invalid";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    } else if (error.getPrimaryError() == SslError.SSL_INVALID) {
                        msg = "A generic error occurred";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    } else if (error.getPrimaryError() == SslError.SSL_EXPIRED) {
                        msg = "The certificate has expired";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    } else if (error.getPrimaryError() == SslError.SSL_IDMISMATCH) {
                        msg = "Hostname mismatch";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    } else if (error.getPrimaryError() == SslError.SSL_NOTYETVALID) {
                        msg = "The certificate is not yet valid";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    } else if (error.getPrimaryError() == SslError.SSL_UNTRUSTED) {
                        msg = "The certificate authority is not trusted";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

              Log.d(TAG, "onPageStarted " + url);
               /* if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }*/
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {

              /*  if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }*/
                super.onPageFinished(view, url);
            }

        });

        mWebview.setWebChromeClient(new WebChromeClient());
        String urltoload = Constants.PATH_TO_HOME_WEBVIEW;
        mWebview.loadUrl(urltoload);

        UserObject userDetails = ((CustomApplication) getApplication()).getLoginUser();
        Log.d(TAG, "MSISDN_"+userDetails.getMsisdn());
        Variables.USER_MSISDN=userDetails.getMsisdn();
        Variables.USER_NAME=userDetails.getUsername();
        getUserCoins(userDetails.getMsisdn());
        mWebview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                mWebview.loadUrl("file:///android_asset/myerrorpage.html");

            }
        });

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    private void getUserCoins(String msisdn) {
        Log.d(TAG, "getUserCoins");
        String URL = Constants.PATH_TO_GET_USER_COIN + "/" + msisdn;
        GsonRequest<UserCoins> serverRequest = new GsonRequest<>(
                Request.Method.POST,
                URL,
                UserCoins.class,
                null,
                getUserCoinsSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Response.Listener<UserCoins>getUserCoinsSuccessListener() {
        Log.d(TAG, "getUserCoins Response");
        return new Response.Listener<UserCoins>() {
            @Override
            public void onResponse(UserCoins response) {
                try {
                    if (response != null) {
                        Log.d(TAG, "getUserCoins : " + response.getUserCoin());
                        Variables.COIN=Integer.parseInt(response.getUserCoin());
                        home_user_coin.setText("Available coins: " + response.getUserCoin());
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    @Override
    public void onRestart()
    {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    private void getMSISDN() {
        Log.d(TAG, "getting MSISDN......");
        GsonRequest<UserObject> serverRequest = new GsonRequest<UserObject>(
                Request.Method.POST,
                Constants.PATH_TO_GRAB_MSISDN,
                UserObject.class,
                createRequestSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private Response.Listener<UserObject> createRequestSuccessListener() {
        return new Response.Listener<UserObject>() {
            @Override
            public void onResponse(UserObject response) {
                try {
                    Log.d(TAG, "getting MSISDN : " + response);
                    if (response != null) {

                        if (response.getMsisdn() != null && !response.getMsisdn().equals("") && !response.getMsisdn().equals("null") && response.getMsisdn().length() > 9) {

                            Intent loginIntent = new Intent(getApplicationContext(), BuyCoinActivity.class);
                            loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(loginIntent);

                        } else {
                            displayErrorMessage(MainActivityHome.this, getString(R.string.need_robi_network));
                        }

                    } else {
                        Log.d(TAG, "getting MSISDN : null");
                        displayErrorMessage(MainActivityHome.this, getString(R.string.need_robi_network));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Override
    protected void onResume() {

        home_user_coin.setText("Available coins: " + Variables.COIN);
        super.onResume();

    }

    @Override
    public void onRefresh() {

    }
}

