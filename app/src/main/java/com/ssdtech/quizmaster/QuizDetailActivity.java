package com.ssdtech.quizmaster;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ssdtech.quizmaster.entities.ScoreObject;
import com.ssdtech.quizmaster.entities.UserCoins;
import com.ssdtech.quizmaster.entities.UserObject;
import com.ssdtech.quizmaster.network.GsonRequest;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.DisplayMessage;
import com.ssdtech.quizmaster.utils.Helper;
import com.ssdtech.quizmaster.utils.Variables;

import java.util.HashMap;
import java.util.Map;

import static com.ssdtech.quizmaster.utils.DisplayMessage.displayErrorMessage;

public class QuizDetailActivity extends AppCompatActivity {

    private static final String TAG = QuizDetailActivity.class.getSimpleName();

    private TextView quizName, quizDescription, quizCompetedPercentage, totalQuestion, timePlayed, bestScore, level;
    private CheckBox quizAgreement;
    private View playButton;

    private UserObject userDetails;
    private String totalQuizCount;
    private String quiz_name;
    TextView questionNumber,required_coin;
    String category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_detail);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        quizName = (TextView) findViewById(R.id.quiz_name);
        quizDescription = (TextView) findViewById(R.id.quiz_description);
        quizCompetedPercentage = (TextView) findViewById(R.id.percentage);

        timePlayed = (TextView) findViewById(R.id.time_played);
        bestScore = (TextView) findViewById(R.id.best_score);
        level = (TextView) findViewById(R.id.level);
        totalQuestion = (TextView) findViewById(R.id.total_question);
        quizAgreement = (CheckBox) findViewById(R.id.agree);
        playButton = (Button) findViewById(R.id.play_button);
        playButton.setVisibility(View.INVISIBLE);
        questionNumber= (TextView) findViewById(R.id.question_no);
        required_coin= (TextView) findViewById(R.id.require_coin);

        if (Helper.isNetworkAvailable(this)) {
            getQuizDetails();
        } else {
            DisplayMessage.displayErrorMessage(this, "No network available");
        }

    }

    private void getQuizDetails() {

        Log.d(TAG, "getQuizDetails");
        userDetails = ((CustomApplication) getApplication()).getLoginUser();

        category = getIntent().getExtras().getString(Constants.CATEGORY);
        Log.d(TAG, "category : " + category);
        Log.d(TAG, "getQuizDetails : " + userDetails.getMsisdn());
        Map params = getParams(userDetails.getMsisdn(),category);
        GsonRequest<ScoreObject> serverRequest = new GsonRequest<ScoreObject>(
                Request.Method.POST,
                Constants.PATH_TO_QUIZ_DETAILS,
                ScoreObject.class,
                params,
                createRequestSuccessListenerMsisdn(),
                createRequestErrorListener());

        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Map getParams(String msisdn,String name) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.MSISDN, msisdn);
        params.put(Constants.CATEGORY_NAME, name);
        return params;
    }

    private Response.Listener<ScoreObject> createRequestSuccessListenerMsisdn() {
        return new Response.Listener<ScoreObject>() {
            @Override
            public void onResponse(final ScoreObject response) {
                try {
                    if (response != null) {
                        Log.d(TAG, "getQuizDetails Result: ");

                        totalQuestion.setText("TOTAL QUESTION IN " + response.getName());

                        quizName.setText(response.getName());
                        quizDescription.setText(response.getDescription());
                        level.setText(String.valueOf(response.getLevel()));
                        Log.d(TAG, "Score " + response.getMaxscore() + " Time " + response.getTotaltime());
                        if (response.getMaxscore() != null) {
                            bestScore.setText(response.getMaxscore());
                        } else {
                            bestScore.setText("0");
                        }

                        if (response.getTotaltime() != null) {
                            timePlayed.setText(response.getTotaltime());
                        } else {
                            timePlayed.setText("0");
                        }

                        if (response.getTotalquiz().equals("0")) {
                            quizCompetedPercentage.setText(response.getTotalquiz() + " QUESTION");
                            questionNumber.setText("3.\tThere are total "+response.getTotalquiz()+" question");
                        } else {
                            questionNumber.setText("3.\tThere are total "+response.getTotalquiz()+" questions");
                        }

                        required_coin.setText("4.\tYou must have "+response.getTotalcoin()+" coins to start");

                        totalQuizCount = response.getTotalquiz();
                        final int totalCoin = Integer.parseInt(response.getTotalcoin());
                        quiz_name = response.getName();
                        playButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // check if there is a quiz in this quiz group
                                int mCount = Integer.parseInt(totalQuizCount);

                                if (mCount <= 0) {
                                    DisplayMessage.displayErrorMessage(QuizDetailActivity.this, "No quiz in this quiz group yet!");
                                    return;
                                }

                                if (quizAgreement.isChecked()) {

                                    if (Variables.COIN < totalCoin) {
                                        Log.d(TAG,"coin"+totalCoin);
                                        Toast.makeText(getApplicationContext(), "You have not enough coin", Toast.LENGTH_SHORT).show();
                                        return;
                                    }

                                    getMSISDN();

                                } else {
                                    DisplayMessage.displayShortMessage(QuizDetailActivity.this, "You must agree to quiz instruction before you start");
                                }
                            }
                        });

                        playButton.setVisibility(View.VISIBLE);

                    } else {
                        displayErrorMessage(QuizDetailActivity.this, "No subcategory found ");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.Listener<UserCoins> getUserCoinsSuccessListener() {
        //com.ssdtech.quizmaster.utils.Log.d(TAG, "getUserCoins Response");
        return new Response.Listener<UserCoins>() {
            @Override
            public void onResponse(UserCoins response) {
                try {
                    if (response != null) {

                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    private void getMSISDN() {
        com.ssdtech.quizmaster.utils.Log.d(TAG, "getting MSISDN......");
        GsonRequest<UserObject> serverRequest = new GsonRequest<UserObject>(
                Request.Method.POST,
                Constants.PATH_TO_GRAB_MSISDN,
                UserObject.class,
                createRequestSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Response.Listener<UserObject> createRequestSuccessListener() {
        return new Response.Listener<UserObject>() {
            @Override
            public void onResponse(UserObject response) {
                try {
                    com.ssdtech.quizmaster.utils.Log.d(TAG, "getting MSISDN : " + response);
                    if (response != null) {
                        com.ssdtech.quizmaster.utils.Log.d(TAG, "getting MSISDN2 : " + response);
                        if (response.getMsisdn() != null && !response.getMsisdn().equals("") && !response.getMsisdn().equals("null") && response.getMsisdn().length() > 9) {
                            com.ssdtech.quizmaster.utils.Log.d(TAG, "getting MSISDN3 : " + response);
                                Intent playIntent = new Intent(QuizDetailActivity.this, QuizActivity.class);
                                playIntent.putExtra(Constants.QUIZ_NAME, category);
                                playIntent.putExtra(Constants.INDICATOR, Constants.FROM_QUIZE_DETAIL);
                                startActivity(playIntent);
                                finish();

                        } else {
                            displayErrorMessage(QuizDetailActivity.this, getString(R.string.need_robi_network));
                        }

                    } else {
                        com.ssdtech.quizmaster.utils.Log.d(TAG, "getting MSISDN : null");
                        displayErrorMessage(QuizDetailActivity.this, getString(R.string.need_robi_network));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }


}
