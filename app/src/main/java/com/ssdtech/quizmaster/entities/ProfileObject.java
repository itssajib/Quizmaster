package com.ssdtech.quizmaster.entities;


public class ProfileObject {

    private String profile;

    public ProfileObject(String profile) {
        this.profile = profile;
    }

    public String getProfile() {
        return profile;
    }
}
