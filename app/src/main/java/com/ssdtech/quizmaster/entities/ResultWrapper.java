package com.ssdtech.quizmaster.entities;


public class ResultWrapper {

    private String questionNumber;
    private String Question;
    private String correct;
    private String wrong;
    private int isAnswer;
    private String time;
    private String point;

    public ResultWrapper(String questionNumber, String question, String correct, String wrong, int isAnswer,String time,String point) {
        this.questionNumber = questionNumber;
        Question = question;
        this.correct = correct;
        this.wrong = wrong;
        this.isAnswer = isAnswer;
        this.time=time;
        this.point=point;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    public String getQuestionNumber() {
        return questionNumber;
    }

    public String getQuestion() {
        return Question;
    }

    public String getCorrect() {
        return correct;
    }

    public String getWrong() {
        return wrong;
    }

    public int getIsAnswer() {
        return isAnswer;
    }
    public String getPoint() {
        return point;
    }
}
