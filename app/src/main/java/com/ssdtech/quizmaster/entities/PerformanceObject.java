package com.ssdtech.quizmaster.entities;


public class PerformanceObject {
    private String name;
    private String value;

    public PerformanceObject(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
