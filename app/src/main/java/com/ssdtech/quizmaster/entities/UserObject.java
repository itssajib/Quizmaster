package com.ssdtech.quizmaster.entities;


public class UserObject {

    private int id;
    private String names;
    private String email;
    private String user_name;
    private String msisdn;
    private String mood_avatar;

    public UserObject(int id, String names, String email, String user_name, String msisdn) {
        this.id = id;
        this.names = names;
        this.email = email;
        this.user_name = user_name;
        this.msisdn = msisdn;
    }

    public UserObject(String names, String email, String user_name, String msisdn, String mood_avatar) {
        this.names = names;
        this.email = email;
        this.user_name = user_name;
        this.msisdn = msisdn;
        this.mood_avatar = mood_avatar;
    }

    public UserObject(String msisdn) {
        this.msisdn = msisdn;
    }

    public int getId() {
        return id;
    }

    public String getNames() {
        return names;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return user_name;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public String getMood_avatar() {
        return mood_avatar;
    }

    @Override
    public String toString() {
        return "id:" + id + "|" + "names:" + names + "|" + "email:" + email + "|" + "user_name:" + user_name + "|" + "msisdn:" + msisdn + "|" + "mood_avatar:" + mood_avatar;
    }
}
