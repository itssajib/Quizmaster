package com.ssdtech.quizmaster.entities;


public class QuestionObject {

    private int id;
    private String subcategory;
    private String question;
    private String option_one;
    private String option_two;
    private String option_three;
    private String option_four;
    private String answer;
    private String image;
    private  String point;
    private  String coins;

    public QuestionObject(int id, String subcategory, String question, String option_one, String option_two,
                          String option_three, String option_four, String answer, String image,String point,String coins) {
        this.id = id;
        this.subcategory = subcategory;
        this.question = question;
        this.option_one = option_one;
        this.option_two = option_two;
        this.option_three = option_three;
        this.option_four = option_four;
        this.answer = answer;
        this.image = image;
        this.point=point;
        this.coins=coins;
    }

    public int getId() {
        return id;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public String getQuestion() {
        return question;
    }

    public String getOption_one() {
        return option_one;
    }

    public String getOption_two() {
        return option_two;
    }

    public String getOption_three() {
        return option_three;
    }

    public String getOption_four() {
        return option_four;
    }

    public String getAnswer() {
        return answer;
    }

    public String getImage() {
        return image;
    }

    public String getPoint() {
        return point;
    }

    public String getCoins() {
        return coins;
    }

}
