package com.ssdtech.quizmaster.entities;


public class SingleQuizObject {

    private int id;
    private String category;
    private String name;
    private String description;
    private String image;

    public SingleQuizObject(int id, String category, String name, String description, String image) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.description = description;
        this.image = image;
    }

    public SingleQuizObject(String category, String name, String description, String image) {
        this.category = category;
        this.name = name;
        this.description = description;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }
}
