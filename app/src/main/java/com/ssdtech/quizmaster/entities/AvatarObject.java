package com.ssdtech.quizmaster.entities;

/**
 * Created by Nazibul on 12/28/2017.
 */

public class AvatarObject {

    private String imagePath;
    private String imageName;

    AvatarObject(String imagePath, String imageName) {
        this.imagePath = imagePath;
        this.imageName = imageName;
    }

    public String getImagePath() {
        return imagePath;
    }
}
