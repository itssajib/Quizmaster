package com.ssdtech.quizmaster.entities;

/**
 * Created by Nazibul on 12/28/2017.
 */

public class ResponseObject {

    private String response;

    ResponseObject(String success) {
        response = success;
    }

    ResponseObject(int success) {
        response = success + "";
    }

    public String getResponse() {
        return response;
    }
}
