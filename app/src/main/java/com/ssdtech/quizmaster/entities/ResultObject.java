package com.ssdtech.quizmaster.entities;


public class ResultObject {

    private String path;
    private String question;
    private String correct;
    private String wrong;

    public ResultObject(String path, String question, String correct, String wrong) {
        this.path = path;
        this.question = question;
        this.correct = correct;
        this.wrong = wrong;
    }

    public String getPath() {
        return path;
    }

    public String getQuestion() {
        return question;
    }

    public String getCorrect() {
        return correct;
    }

    public String getWrong() {
        return wrong;
    }
}
