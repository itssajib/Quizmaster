package com.ssdtech.quizmaster.entities;


public class ScoreObject {

    private String totaltime;
    private String maxscore;
    private String totalquiz;
    private String level;
    private String name;
    private String description;
    private String totalcoin;

    public ScoreObject(String totaltime, String maxscore, String totalquiz,String totalcoin) {
        this.totaltime = totaltime;
        this.maxscore = maxscore;
        this.totalquiz = totalquiz;
        this.totalcoin=totalcoin;
    }

    public ScoreObject(String totaltime, String maxscore, String totalquiz, String name, String description, String level,String totalcoin) {
        this.totaltime = totaltime;
        this.maxscore = maxscore;
        this.totalquiz = totalquiz;
        this.level = level;
        this.name = name;
        this.description = description;
        this.totalcoin=totalcoin;
    }

    public String getTotaltime() {
        return totaltime;
    }

    public String getMaxscore() {
        return maxscore;
    }

    public String getTotalquiz() {
        return totalquiz;
    }

    public String getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }
    public String getTotalcoin() {
        return totalcoin;
    }
    public String getDescription() {
        return description;
    }
}
