package com.ssdtech.quizmaster.entities;

/**
 * Created by Nazibul on 12/30/2017.
 */

public class UserCoins {

    private String user_coin;

    UserCoins(String user_coin) {
        this.user_coin = user_coin;
    }

    public String getUserCoin() {
        return user_coin;
    }
}
