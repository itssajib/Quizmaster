package com.ssdtech.quizmaster.entities;


public class CheckObject{

    private int id;
    private boolean state;
    private String name;
    private String image;

    public CheckObject(int id, boolean state, String name, String image) {
        this.id = id;
        this.state = state;
        this.name = name;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public boolean isState() {
        return state;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }
}
