package com.ssdtech.quizmaster.entities;


public class TopScoreObject {

    private String scores;
    private String username;

    public TopScoreObject(String scores, String username) {
        this.scores = scores;
        this.username = username;
    }

    public String getScores() {
        return scores;
    }

    public String getUsername() {
        return username;
    }
}
