package com.ssdtech.quizmaster.entities;

/**
 * Created by Sajib on 1/8/2018.
 */

public class UserPosition {
    private String position;
    private String point;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }


}
