package com.ssdtech.quizmaster;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;
import com.ssdtech.quizmaster.adapters.ResultAdapter;
import com.ssdtech.quizmaster.entities.ResultWrapper;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AnalysisActivity extends AppCompatActivity {

    private static final String TAG = AnalysisActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analysis);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        String mResult = getIntent().getExtras().getString(Constants.QUIZ_RESULT);
        Gson gson = ((CustomApplication) getApplication()).getGsonObject();
        ResultWrapper[] mWrapper = gson.fromJson(mResult, ResultWrapper[].class);
        List<ResultWrapper> submittedResult = arrayToListObject(mWrapper);

        RecyclerView resultRecyclerView = (RecyclerView)findViewById(R.id.quiz_result);
        resultRecyclerView.setLayoutManager(new LinearLayoutManager(AnalysisActivity.this));
        resultRecyclerView.setHasFixedSize(true);

        ResultAdapter mAdapter = new ResultAdapter(submittedResult);
        resultRecyclerView.setAdapter(mAdapter);
    }

    private List<ResultWrapper> arrayToListObject(ResultWrapper[] response){
        List<ResultWrapper> allCategories = new ArrayList<>();
        Collections.addAll(allCategories, response);
        return allCategories;
    }
}
