package com.ssdtech.quizmaster;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ssdtech.quizmaster.entities.UserObject;
import com.ssdtech.quizmaster.network.GsonRequest;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.DisplayMessage;
import com.ssdtech.quizmaster.utils.Helper;

import java.util.HashMap;
import java.util.Map;

import static com.ssdtech.quizmaster.utils.DisplayMessage.displayErrorMessage;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private EditText email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        ((CustomApplication) getApplication()).redirectLoginUsers();
        setContentView(R.layout.activity_login);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);

        Button loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mEmail = email.getText().toString();
                String mPassword = password.getText().toString();

                if (TextUtils.isEmpty(mPassword)) {
                    displayErrorMessage(LoginActivity.this, "Email and password fields must be filled");
                } else {

                    if (Helper.isNetworkAvailable(LoginActivity.this)) {
                        loginCallToServer(mEmail, mPassword);
                    } else {
                        DisplayMessage.displayErrorMessage(LoginActivity.this, "No network available");
                    }
                }
            }
        });

        TextView signUpText = (TextView) findViewById(R.id.sign_up_link);
        signUpText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signUpIntent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(signUpIntent);
            }
        });
    }

    private void loginCallToServer(String email, String password) {
        Map params = getParams(email, password);
        GsonRequest<UserObject> serverRequest = new GsonRequest<UserObject>(
                Request.Method.POST,
                Constants.PATH_TO_SERVER_LOGIN,
                UserObject.class,
                params,
                createRequestSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Map getParams(String email, String password) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.EMAIL, email);
        params.put(Constants.PASSWORD, password);
        return params;
    }

    private Response.Listener<UserObject> createRequestSuccessListener() {
        return new Response.Listener<UserObject>() {
            @Override
            public void onResponse(UserObject response) {
                try {
                    if (response != null) {
                        //save user login data to a shared preference
                        String userData = ((CustomApplication) getApplication()).getGsonObject().toJson(response);
                        ((CustomApplication) getApplication()).getShared().setUserData(userData);
                        Intent selectQuizIntent = new Intent(LoginActivity.this, MainActivityHome.class);
                        startActivity(selectQuizIntent);

                    } else {

                        displayErrorMessage(LoginActivity.this, "Invalid email or password");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }
}

