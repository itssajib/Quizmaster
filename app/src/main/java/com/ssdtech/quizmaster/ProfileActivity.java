package com.ssdtech.quizmaster;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ssdtech.quizmaster.entities.UserCoins;
import com.ssdtech.quizmaster.entities.UserObject;
import com.ssdtech.quizmaster.entities.UserPosition;
import com.ssdtech.quizmaster.network.GsonRequest;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.Log;

import static com.ssdtech.quizmaster.utils.DisplayMessage.displayErrorMessage;

public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = HistoryActivity.class.getSimpleName();

    private ImageView profileImage;
    private TextView profileName;
    private TextView profileUserName;
    private TextView profileEmail;
    private TextView userCoin, position, point;
    private View profileEditBtn, buycoin, play, backbtn, social_share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        social_share = findViewById(R.id.social_share);

        profileImage = (ImageView) findViewById(R.id.profileImage);
        profileName = (TextView) findViewById(R.id.profileName);
        profileEmail = (TextView) findViewById(R.id.profileEmail);
        profileUserName = (TextView) findViewById(R.id.profileUsername);
        profileEditBtn = findViewById(R.id.profileEditBtn);
        userCoin = (TextView) findViewById(R.id.userCoin);
        point = (TextView) findViewById(R.id.point);
        position = (TextView) findViewById(R.id.position);
        buycoin = findViewById(R.id.buycoin);
        play = findViewById(R.id.play_quiz);

        buycoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isOnline()){
                    Log.d(TAG,"net_available");
                    getMSISDN();
                }else{
                    Toast.makeText(getApplicationContext(),getString(R.string.need_robi_network),Toast.LENGTH_LONG).show();
                }

            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(getApplicationContext(), QuizDetailActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loginIntent);
            }
        });

        UserObject userDetails = ((CustomApplication) getApplication()).getLoginUser();
        Log.d(TAG, userDetails.getMsisdn());
        getUserPosition("" + userDetails.getId());

        profileName.setText(userDetails.getNames());
        profileUserName.setText(userDetails.getUsername());
        profileEmail.setText(userDetails.getEmail());
        Glide.with(ProfileActivity.this).load(Constants.PUBLIC_FOLDER + userDetails.getMood_avatar()).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().into(profileImage);

        profileEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(getApplicationContext(), AvatarActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                loginIntent.putExtra(Constants.INDICATOR, "FROM_EDIT");
                startActivity(loginIntent);
            }
        });

        social_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.ssdtech.quizmaster&hl=en");
                startActivity(Intent.createChooser(intent, "Share"));
            }
        });

        getUserCoins(userDetails.getMsisdn());

        backbtn = findViewById(R.id.backbtn);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void getUserCoins(String msisdn) {
        Log.d(TAG, "getUserCoins");
        String URL = Constants.PATH_TO_GET_USER_COIN + "/" + msisdn;
        GsonRequest<UserCoins> serverRequest = new GsonRequest<>(
                Request.Method.POST,
                URL,
                UserCoins.class,
                null,
                getUserCoinsSuccessListener(),
                createRequestErrorListener());
        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Response.Listener<UserCoins> getUserCoinsSuccessListener() {
        Log.d(TAG, "getUserCoins Response");
        return new Response.Listener<UserCoins>() {
            @Override
            public void onResponse(UserCoins response) {
                try {
                    if (response != null && response.getUserCoin()!=null && !response.getUserCoin().equals("")) {
                        Log.d(TAG, "getUserCoins : " + response.getUserCoin());
                        if (response.getUserCoin() != null) {

                            userCoin.setText("Coins available: " + response.getUserCoin());

                        }else{
                           // Toast.makeText(getApplicationContext(),"Hey! you haven't played any quiz today. Start playing quiz to win todays gifts.",Toast.LENGTH_LONG).show();
                        }
                    } else {
                        //Toast.makeText(getApplicationContext(),"Hey! you haven't played any quiz today. Start playing quiz to win todays gifts.",Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    private void getUserPosition(String userid) {
        Log.d(TAG, "getUserCoins");
        String URL = Constants.PATH_TO_GET_USER_POSITION + "/" + userid;
        GsonRequest<UserPosition> serverRequest = new GsonRequest<>(
                Request.Method.POST,
                URL,
                UserPosition.class,
                null,
                getUserPositionSuccessListener(),
                createRequestErrorListener());
        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Response.Listener<UserPosition> getUserPositionSuccessListener() {
        Log.d(TAG, "getUserCoins Response");
        return new Response.Listener<UserPosition>() {
            @Override
            public void onResponse(UserPosition response) {
                try {
                    if (response != null && response.getPosition()!=null) {
                        // Log.d(TAG, "getUserCoins : " + response.getUserCoin());
                        position.setText("" + response.getPosition());
                        point.setText("" + response.getPoint());
                    } else {
                        Toast.makeText(getApplicationContext(),"Hey! you haven't played any quiz today. Start playing quiz to win today's gifts.",Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private void getMSISDN() {
        Log.d(TAG, "getting MSISDN......");
        GsonRequest<UserObject> serverRequest = new GsonRequest<UserObject>(
                Request.Method.POST,
                Constants.PATH_TO_GRAB_MSISDN,
                UserObject.class,
                createRequestSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private Response.Listener<UserObject> createRequestSuccessListener() {
        return new Response.Listener<UserObject>() {
            @Override
            public void onResponse(UserObject response) {
                try {
                    Log.d(TAG, "getting MSISDN : " + response);
                    if (response != null) {

                        if (response.getMsisdn() != null && !response.getMsisdn().equals("") && !response.getMsisdn().equals("null") && response.getMsisdn().length() > 9) {

                            Intent loginIntent = new Intent(getApplicationContext(), BuyCoinActivity.class);
                            loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(loginIntent);

                        } else {
                            displayErrorMessage(ProfileActivity.this, getString(R.string.need_robi_network));
                        }

                    } else {
                        Log.d(TAG, "getting MSISDN : null");
                        displayErrorMessage(ProfileActivity.this, getString(R.string.need_robi_network));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }


}
