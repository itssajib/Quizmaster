package com.ssdtech.quizmaster;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ssdtech.quizmaster.entities.UserObject;
import com.ssdtech.quizmaster.network.GsonRequest;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.DisplayMessage;
import com.ssdtech.quizmaster.utils.Helper;
import com.ssdtech.quizmaster.utils.Log;

import org.apache.commons.validator.routines.EmailValidator;

import java.util.HashMap;
import java.util.Map;

import static com.ssdtech.quizmaster.utils.DisplayMessage.displayErrorMessage;

public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = SignUpActivity.class.getSimpleName();

    private View msisdnarea;
    private EditText email, username, msisdn, names;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        ((CustomApplication) getApplication()).redirectLoginUsers();
        setContentView(R.layout.activity_sign_up_2);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        String usermsisdn = getIntent().getStringExtra(Constants.MSISDN);

        email = (EditText) findViewById(R.id.email);
        username = (EditText) findViewById(R.id.user_name);
        msisdn = (EditText) findViewById(R.id.mobile_number);
        names = (EditText) findViewById(R.id.name);
        msisdnarea = findViewById(R.id.mobile_number_area);

        final CheckBox agreeBox = (CheckBox) findViewById(R.id.agree_box);

        if (usermsisdn.length() > 9) {
            usermsisdn = "880" + usermsisdn.substring(usermsisdn.length() - 10);
            msisdn.setText(usermsisdn);
            msisdnarea.setVisibility(View.GONE);
        }

        View signUpButton = findViewById(R.id.sign_up_button);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "signUpButton Pressed");
                if (!agreeBox.isChecked()) {
                    displayErrorMessage(SignUpActivity.this, "You must agree to our terms and policies");
                    return;
                }

                EmailValidator validator = EmailValidator.getInstance();

                String mEmail = email.getText().toString();
                String mUsername = username.getText().toString();
                String mNames = names.getText().toString();
                String mMsisdn = msisdn.getText().toString();

                if (mMsisdn.length() > 9) {
                    mMsisdn = "880" + mMsisdn.substring(mMsisdn.length() - 10);
                }

                /*if (!validator.isValid(mEmail)) {
                    DisplayMessage.displayErrorMessage(SignUpActivity.this, "You have entered an invalid email");
                    return;
                }
                */
               /* if (mEmail.length() < 12 || mNames.length() < 6) {
                    displayErrorMessage(SignUpActivity.this, "email or name length is to short");
                    return;
                }*/

                if (TextUtils.isEmpty(mMsisdn) || TextUtils.isEmpty(mUsername)) {
                    displayErrorMessage(SignUpActivity.this, "All input fields must be filled");
                    return;
                }

                if (Helper.isNetworkAvailable(SignUpActivity.this)) {
                    createNewAccount(mEmail, mUsername, mNames, mMsisdn);
                } else {
                    displayErrorMessage(SignUpActivity.this, getString(R.string.need_robi_network));
                }
            }
        });

    }

    private void createNewAccount(String email, String username, String names, String msisdn) {
        Log.d(TAG, "createNewAccount");
        Map params = getParams(email, username, names, msisdn);
        GsonRequest<UserObject> serverRequest = new GsonRequest<UserObject>(
                Request.Method.POST,
                Constants.PATH_TO_SERVER_SIGN_IN,
                UserObject.class,
                params,
                createRequestSuccessListener(),
                createRequestErrorListener());

        ((CustomApplication) getApplication()).getNetworkCall().callToRemoteServer(serverRequest);
    }

    private Map getParams(String email, String username, String names, String msisdn) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.EMAIL, email);
        params.put(Constants.USERNAME, username);
        params.put(Constants.MSISDN, msisdn);
        params.put(Constants.NAMES, names);
        return params;
    }

    private Response.Listener<UserObject> createRequestSuccessListener() {
        return new Response.Listener<UserObject>() {
            @Override
            public void onResponse(UserObject response) {
                try {
                    if (response != null) {
                        Log.d(TAG, "createNewAccount : " + response);
                        //save user login data to a shared preference
                        String userData = ((CustomApplication) getApplication()).getGsonObject().toJson(response);
                        ((CustomApplication) getApplication()).getShared().setUserData(userData);
                        Intent loginIntent = new Intent(getApplicationContext(), AvatarActivity.class);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        loginIntent.putExtra(Constants.INDICATOR,Constants.FROM_SIGNUP);
                        startActivity(loginIntent);
                        finish();
                    } else {
                        displayErrorMessage(SignUpActivity.this, "Error! Registration failed. Try again");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }
}
