package com.ssdtech.quizmaster.interfaces;

/**
 * Created by Sajib on 1/8/2018.
 */

public interface SendScoreListener {

    void sendDataToServer();
}
