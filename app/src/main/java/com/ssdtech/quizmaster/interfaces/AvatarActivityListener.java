package com.ssdtech.quizmaster.interfaces;

/**
 * Created by Nazibul on 12/28/2017.
 */

public interface AvatarActivityListener {
    void onSelectedImagePath(String path);
}
