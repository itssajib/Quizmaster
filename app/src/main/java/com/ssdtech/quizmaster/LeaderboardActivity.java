package com.ssdtech.quizmaster;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ssdtech.quizmaster.entities.UserObject;
import com.ssdtech.quizmaster.interfaces.ActivityInteractionListener;
import com.ssdtech.quizmaster.utils.Constants;
import com.ssdtech.quizmaster.utils.CustomApplication;
import com.ssdtech.quizmaster.utils.JavaScriptInterface;
import com.ssdtech.quizmaster.utils.Log;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class LeaderboardActivity extends AppCompatActivity implements ActivityInteractionListener{

    private static final String TAG = LeaderboardActivity.class.getSimpleName();

    private WebView mWebview;
    private View backbtn,social_share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_leaderboard);

        social_share=findViewById(R.id.social_share);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        mWebview = (WebView) findViewById(R.id.leaderboard_webview);

        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.getSettings().setPluginState(WebSettings.PluginState.ON);
        //mWebview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mWebview.getSettings().setDomStorageEnabled(true);
        mWebview.getSettings().setDatabaseEnabled(true);
        mWebview.setLongClickable(true);
        mWebview.getSettings().setLoadWithOverviewMode(true);
        //mWebview.getSettings().setUseWideViewPort(true);
        mWebview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        // mWebview.getSettings().setBuiltInZoomControls(true);
        //mWebview.getSettings().setDisplayZoomControls(true);
        mWebview.addJavascriptInterface(new JavaScriptInterface(LeaderboardActivity.this,this), "Android");

        mWebview.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                //handler.proceed();
                String msg = "";
                final AlertDialog.Builder builder = new AlertDialog.Builder(LeaderboardActivity.this);

                if (error.getPrimaryError() == SslError.SSL_DATE_INVALID
                        || error.getPrimaryError() == SslError.SSL_EXPIRED
                        || error.getPrimaryError() == SslError.SSL_IDMISMATCH
                        || error.getPrimaryError() == SslError.SSL_INVALID
                        || error.getPrimaryError() == SslError.SSL_NOTYETVALID
                        || error.getPrimaryError() == SslError.SSL_UNTRUSTED) {
                    if (error.getPrimaryError() == SslError.SSL_DATE_INVALID) {
                        msg = "The date of the certificate is invalid";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    } else if (error.getPrimaryError() == SslError.SSL_INVALID) {
                        msg = "A generic error occurred";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    } else if (error.getPrimaryError() == SslError.SSL_EXPIRED) {
                        msg = "The certificate has expired";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    } else if (error.getPrimaryError() == SslError.SSL_IDMISMATCH) {
                        msg = "Hostname mismatch";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    } else if (error.getPrimaryError() == SslError.SSL_NOTYETVALID) {
                        msg = "The certificate is not yet valid";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    } else if (error.getPrimaryError() == SslError.SSL_UNTRUSTED) {
                        msg = "The certificate authority is not trusted";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.d(TAG, "onPageStarted " + url);
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }

        });

        mWebview.setWebChromeClient(new WebChromeClient());

        social_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.ssdtech.quizmaster&hl=en");
                startActivity(Intent.createChooser(intent, "Share"));
            }
        });

        UserObject userDetails = ((CustomApplication) getApplication()).getLoginUser();
        android.util.Log.d(TAG, userDetails.getMsisdn());
        String urltoload = Constants.PATH_TO_LEADERBOARD_WEBVIEW + "/" + userDetails.getMsisdn();
        Log.d(TAG,"leaderboard_url:"+urltoload);
        mWebview.loadUrl(urltoload);
        backbtn = findViewById(R.id.backbtn);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onRefresh() {

    }
}
